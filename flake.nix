{
  description = "Book Club Calendar";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    rust-overlay,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        overlays = [(import rust-overlay)];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
      in {
        devShells.default = pkgs.mkShell {
          buildInputs = [
            (pkgs.rust-bin.stable."1.81.0".default.override {
              extensions = ["rust-src" "rust-analyzer"];
            })
            pkgs.pkg-config
            pkgs.alejandra
            pkgs.atkmm
            pkgs.cairo
            pkgs.gdk-pixbuf
            pkgs.glib
            pkgs.gtk4
            pkgs.wrapGAppsHook3
            pkgs.pango

            pkgs.cargo-tarpaulin

            pkgs.rgbds
          ];
          # Fix GSettings crash on NixOS
          GSETTINGS_SCHEMA_DIR = "${pkgs.gtk3}/share/gsettings-schemas/${pkgs.gtk3.name}/glib-2.0/schemas";
        };
        formatter = pkgs.alejandra;
      }
    );
}
