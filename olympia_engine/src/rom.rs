//! ROM and Cartridge handling code

use alloc::vec::Vec;
use core::convert::TryFrom;
use core::ops::Range;
use derive_more::derive::Error;
use derive_more::Display;
use enum_dispatch::enum_dispatch;
use olympia_core::address::{regions, Bank, BankOffsetAddress, CartridgeBank, LiteralAddress};

const TARGET_CONSOLE_LOCATION: usize = 0x143;
const CARTRIDGE_TYPE_LOCATION: usize = 0x147;
const ROM_SIZE_LOCATION: usize = 0x148;
const RAM_SIZE_LOCATION: usize = 0x149;

#[derive(PartialEq, Eq, Debug, Display, Error)]
/// Error turning ROMs into cartridges
pub enum CartridgeLoadError {
    /// The ROM's cartridge type (at 0x147) is not known or supported
    #[display("Unsupported cartridge type: 0x{:X}", _0)]
    UnsupportedCartridgeType(#[error(not(source))] u8),
    /// The ROM's RAM size (at 0x149) is not known or supported
    #[display("Unsupported cartridge RAM size: 0x{:X}", _0)]
    UnsupportedRamSize(#[error(not(source))] u8),
    #[display(
        "Data for cartridge too small. Was 0x{:X} bytes, must be at least 0x200",
        _0
    )]
    /// The ROM data is smaller than the cartridge header, and likely corrupt
    CartridgeTooSmall(#[error(not(source))] usize),
}

#[derive(PartialEq, Eq, Debug, Display, Error)]
/// Cartridge Read/Write errors
pub enum CartridgeIOError {
    /// Attempted IO to an address in cart RAM address space not filled on this cart
    #[display("Address 0x{:X} outside of available cart ram", _0)]
    ExceedsCartridgeRam(#[error(not(source))] LiteralAddress),
    /// Attempted IO to an address in cart ROM address space not filled on this cart
    #[display("Address 0x{:X} exceeds ROM", _0)]
    NoDataInRom(#[error(not(source))] LiteralAddress),
    /// Attempted IO to an address not in cart address space
    #[display("Cannot read non-cart address 0x{:X} from cartridge", _0)]
    NonCartAddress(#[error(not(source))] LiteralAddress),
    /// Attempted read from a bank not present on cart
    #[display("No such bank {:?} on cartridge", _0)]
    InvalidBank(#[error(not(source))] Bank),
    /// Attempted IO to cart RAM address space when this cart has no RAM
    #[display("RAM not supported by current cartridge")]
    NoCartridgeRam,
    /// Attempted IO to cart RAM address space when cart RAM is disabled at runtime
    #[display("RAM disabled on current cartridge")]
    CartridgeRamDisabled,
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
/// Indicates if a ROM uses GameBoy Color features
pub enum TargetConsole {
    /// The ROM does not use Color features
    GameBoyOnly,
    /// The ROM uses Color features where supported
    ColorEnhanced,
    /// The ROM requires Color features
    ColorOnly,
}

/// Result of cartridge load operations
pub type CartridgeLoadResult<T> = Result<T, CartridgeLoadError>;
/// Result of cartridge read/write operations
pub type CartridgeIOResult<T> = Result<T, CartridgeIOError>;

pub struct CartridgeRam {
    data: Vec<u8>,
    banks: usize,
    bank_size: usize,
    read_mask: u8,
}

impl CartridgeRam {
    /// Create RAM of a custom number of banks and size
    pub fn new(banks: usize, bank_size: usize) -> CartridgeRam {
        let data = vec![0x00; banks * bank_size];
        CartridgeRam {
            banks,
            bank_size,
            data,
            read_mask: 0xFF,
        }
    }

    /// Create RAM from a Gameboy RAM size code
    pub fn from_ram_size_code(code: u8) -> Result<Option<CartridgeRam>, CartridgeLoadError> {
        match code {
            0 => Ok(None),
            1 => Ok(Some(CartridgeRam::new(1, 2 * 1024))),
            2 => Ok(Some(CartridgeRam::new(1, 0x2000))),
            3 => Ok(Some(CartridgeRam::new(4, 0x2000))),
            4 => Ok(Some(CartridgeRam::new(16, 0x2000))),
            5 => Ok(Some(CartridgeRam::new(8, 0x2000))),
            _ => Err(CartridgeLoadError::UnsupportedRamSize(code)),
        }
    }

    /// Create RAM for a MBC2 type cartidge
    ///
    /// MBC2 RAM is only 4 bits wide, compared to regular 8-bit RAM,
    /// and comes in a single 512 byte size only
    pub fn new_mbc2() -> CartridgeRam {
        let data = vec![0x00; 512];
        CartridgeRam {
            banks: 1,
            bank_size: 512,
            data,
            read_mask: 0x0F,
        }
    }

    pub fn read(&self, bank: u8, offset: BankOffsetAddress) -> u8 {
        let bank = usize::from(bank);
        let offset = usize::from(offset.0);
        self.data[(bank * self.bank_size) + offset] & self.read_mask
    }

    pub fn write(&mut self, bank: u8, offset: BankOffsetAddress, value: u8) {
        let bank = usize::from(bank);
        let offset = usize::from(offset.0);
        self.data[(bank * self.bank_size) + offset] = value & self.read_mask
    }

    pub fn size(&self) -> usize {
        self.data.len()
    }

    pub fn banks(&self) -> usize {
        self.banks
    }
}

/// A gameboy cartridge, including ROM data and memory controller
pub struct Cartridge {
    pub data: Vec<u8>,
    pub controller: ControllerEnum,
    pub target: TargetConsole,
    pub ram: Option<CartridgeRam>,
}

impl Cartridge {
    /// Read a byte from a given bank, even if not currently mapped
    pub fn read_banked(&self, _bank: CartridgeBank, _offset: BankOffsetAddress) {
        todo!("Implement")
    }

    /// Read a byte from address space controlled by the cart
    pub fn read(&self, loc: LiteralAddress) -> CartridgeIOResult<u8> {
        if regions::STATIC_ROM.contains(loc) {
            self.controller.read_static_rom(loc, &self.data)
        } else if regions::SWITCHABLE_ROM.contains(loc) {
            self.controller.read_switchable_rom(loc, &self.data)
        } else if regions::CARTRIDGE_RAM.contains(loc) {
            match self.ram {
                Some(ref ram) => self.controller.read_switchable_ram(loc, ram),
                _ => Err(CartridgeIOError::NoCartridgeRam),
            }
        } else {
            Err(CartridgeIOError::NonCartAddress(loc))
        }
    }

    /// Write a byte to address space controlled by the cart
    pub fn write(&mut self, loc: LiteralAddress, value: u8) -> CartridgeIOResult<()> {
        self.controller.write(loc, value, self.ram.as_mut())
    }

    /// Build a cartridge from ROM data
    pub fn from_data(data: Vec<u8>) -> CartridgeLoadResult<Cartridge> {
        if data.len() < 0x200 {
            return Err(CartridgeLoadError::CartridgeTooSmall(data.len()));
        }
        let cartridge_type_id = data[CARTRIDGE_TYPE_LOCATION];
        let rom_size_byte = data[ROM_SIZE_LOCATION];
        let rom_banks = if rom_size_byte == 0 {
            2u16
        } else {
            2u16.pow(rom_size_byte.into())
        };
        let ram = match cartridge_type_id {
            0x02 | 0x03 | 0x10 | 0x12 | 0x13 => {
                CartridgeRam::from_ram_size_code(data[RAM_SIZE_LOCATION])?
            }
            0x05 | 0x06 => Some(CartridgeRam::new_mbc2()),
            _ => None,
        };
        let target = lookup_target(data[TARGET_CONSOLE_LOCATION]);
        let controller = match cartridge_type_id {
            0 => StaticRom.into(),
            1..=3 => MBC1::new(rom_banks, cartridge_type_id).into(),
            5 | 6 => MBC2::new(cartridge_type_id).into(),
            0x10..=0x13 => MBC3::new(cartridge_type_id).into(),
            _ => {
                return Err(CartridgeLoadError::UnsupportedCartridgeType(
                    cartridge_type_id,
                ))
            }
        };
        Ok(Cartridge {
            controller,
            data,
            target,
            ram,
        })
    }
}

/// Type of cartidge controller
#[enum_dispatch]
pub enum ControllerEnum {
    /// No controller, just static data
    StaticRom,
    /// Uses the MBC 1 controller chip
    Type1(MBC1),
    /// Uses the MBC 2 controller chip
    Type2(MBC2),
    /// Uses the MBC 3 controller chip
    Type3(MBC3),
}

/// Represents a cartridge controller
#[enum_dispatch(ControllerEnum)]
pub trait CartridgeController {
    /// Read a value from the controller's static ROM
    fn read_static_rom(&self, loc: LiteralAddress, rom: &[u8]) -> CartridgeIOResult<u8>;
    /// Read a value from the controller's switchable ROM banks
    fn read_switchable_rom(&self, loc: LiteralAddress, rom: &[u8]) -> CartridgeIOResult<u8>;
    /// Read a value from the controller's switchable RAM banks
    fn read_switchable_ram(&self, loc: LiteralAddress, ram: &CartridgeRam)
        -> CartridgeIOResult<u8>;
    /// Read a value from a ROM bank, regardless of it it's currently mapped
    ///
    /// This will reveal values not accessible to the gameboy normally and should only be
    /// used by debug tools
    fn read_banked_rom(
        &self,
        bank: u16,
        offset: BankOffsetAddress,
        rom: &[u8],
    ) -> CartridgeIOResult<u8> {
        if bank < self.rom_banks() {
            Ok(rom[usize::from((0x2000 * bank) + offset.0)])
        } else {
            Err(CartridgeIOError::InvalidBank(Bank::cartridge_rom(bank)))
        }
    }
    /// Write a value to the controller's memory space
    fn write(
        &mut self,
        loc: LiteralAddress,
        value: u8,
        ram: Option<&mut CartridgeRam>,
    ) -> CartridgeIOResult<()>;
    /// Indicates if a controller contains battery backed RAM
    fn has_battery(&self) -> bool {
        false
    }
    /// Indicates if a controller contains battery backed timer
    fn has_timer(&self) -> bool {
        false
    }
    // Number of ROM banks available
    fn rom_banks(&self) -> u16 {
        1
    }
}

/// A cartridge that contains only a static ROM w/o controller
pub struct StaticRom;

impl CartridgeController for StaticRom {
    fn read_static_rom(&self, loc: LiteralAddress, rom: &[u8]) -> CartridgeIOResult<u8> {
        rom.get(usize::from(loc))
            .copied()
            .ok_or(CartridgeIOError::NoDataInRom(loc))
    }

    fn read_switchable_rom(&self, loc: LiteralAddress, rom: &[u8]) -> CartridgeIOResult<u8> {
        self.read_static_rom(loc, rom)
    }

    fn read_switchable_ram(
        &self,
        _loc: LiteralAddress,
        _ram: &CartridgeRam,
    ) -> CartridgeIOResult<u8> {
        Err(CartridgeIOError::NoCartridgeRam)
    }

    fn write(
        &mut self,
        _loc: LiteralAddress,
        _value: u8,
        _ram: Option<&mut CartridgeRam>,
    ) -> CartridgeIOResult<()> {
        Ok(())
    }
}

#[derive(PartialEq, Eq, Debug)]
enum MBC1PageMode {
    LargeRom,
    LargeRam,
}

/// MBC1 cartridge controller
pub struct MBC1 {
    page_mode: MBC1PageMode,
    selected_rom: u8,
    selected_high: u8,
    ram_enabled: bool,
    has_ram: bool,
    has_battery: bool,
    rom_banks: u16,
}

impl MBC1 {
    pub fn new(rom_banks: u16, cartridge_type_id: u8) -> MBC1 {
        let has_ram = (cartridge_type_id & 0b10) != 0;
        let has_battery = (cartridge_type_id & 0b11) == 0b11;
        MBC1 {
            page_mode: MBC1PageMode::LargeRom,
            selected_rom: 1,
            selected_high: 0,
            ram_enabled: false,
            has_ram,
            has_battery,
            rom_banks,
        }
    }

    fn selected_rom_bank(&self) -> u8 {
        let mut bank_id = self.selected_rom & 0x1F;
        if self.page_mode == MBC1PageMode::LargeRom {
            bank_id |= self.selected_high << 5;
        }
        bank_id
    }

    fn selected_static_rom_bank(&self) -> u8 {
        if self.page_mode == MBC1PageMode::LargeRam {
            0
        } else {
            self.selected_high << 5
        }
    }

    fn selected_ram_bank(&self) -> u8 {
        if self.page_mode == MBC1PageMode::LargeRom {
            0
        } else {
            self.selected_high
        }
    }

    const fn ram_enable_area() -> Range<u16> {
        0x0000..0x2000
    }

    const fn rom_select_area() -> Range<u16> {
        0x2000..0x4000
    }

    const fn high_select_area() -> Range<u16> {
        0x4000..0x6000
    }

    const fn mode_select_area() -> Range<u16> {
        0x6000..0x8000
    }
}

impl CartridgeController for MBC1 {
    fn read_static_rom(&self, loc: LiteralAddress, rom: &[u8]) -> CartridgeIOResult<u8> {
        let bank = usize::from(self.selected_static_rom_bank());
        let rom_addr = (bank * usize::from(regions::STATIC_ROM.len())) + usize::from(loc);
        rom.get(rom_addr)
            .copied()
            .ok_or(CartridgeIOError::NoDataInRom(loc))
    }

    fn read_switchable_rom(&self, loc: LiteralAddress, rom: &[u8]) -> CartridgeIOResult<u8> {
        let bank_addr = loc.region_offset(regions::SWITCHABLE_ROM);
        let bank = usize::from(self.selected_rom_bank());
        let rom_addr = (bank * usize::from(regions::SWITCHABLE_ROM.len())) + usize::from(bank_addr);
        rom.get(rom_addr)
            .copied()
            .ok_or(CartridgeIOError::NoDataInRom(loc))
    }

    fn read_switchable_ram(
        &self,
        loc: LiteralAddress,
        ram: &CartridgeRam,
    ) -> CartridgeIOResult<u8> {
        if self.has_ram && self.ram_enabled {
            let bank = self.selected_ram_bank();
            let ram_addr = loc.region_offset(regions::CARTRIDGE_RAM);
            Ok(ram.read(bank, ram_addr))
        } else if self.has_ram {
            Err(CartridgeIOError::CartridgeRamDisabled)
        } else {
            Err(CartridgeIOError::NoCartridgeRam)
        }
    }

    fn rom_banks(&self) -> u16 {
        self.rom_banks
    }

    fn write(
        &mut self,
        loc: LiteralAddress,
        value: u8,
        ram: Option<&mut CartridgeRam>,
    ) -> CartridgeIOResult<()> {
        if MBC1::ram_enable_area().contains(&loc.0) {
            self.ram_enabled = value == 0b1010;
            tracing::info!(target: "rom::mbc1", new_state = self.ram_enabled, "Toggled RAM enabled: {}", self.ram_enabled);
            Ok(())
        } else if MBC1::rom_select_area().contains(&loc.0) {
            self.selected_rom = value & 0x1F;
            if self.selected_rom == 0 {
                self.selected_rom = 1
            }
            tracing::info!(target: "rom::mbc1", rom_bank=self.selected_rom_bank(), "Selected Cart ROM bank: {}", self.selected_rom_bank());
            Ok(())
        } else if MBC1::high_select_area().contains(&loc.0) {
            self.selected_high = value & 0x3;
            if self.page_mode == MBC1PageMode::LargeRom {
                tracing::info!(
                    target: "rom::mbc1",
                    selected_static_rom_bank = self.selected_static_rom_bank(),
                    cart_rom_bank = self.selected_rom_bank(),
                    "Selected Static ROM bank"
                );
            } else {
                tracing::info!(
                    target: "rom::mbc1",
                    selected_ram_page = self.selected_ram_bank(),
                    cart_rom_bank = self.selected_rom_bank(),
                    "Selected RAM page"
                );
            }
            Ok(())
        } else if MBC1::mode_select_area().contains(&loc.0) {
            if value == 0x00 {
                self.page_mode = MBC1PageMode::LargeRom
            } else {
                self.page_mode = MBC1PageMode::LargeRam
            }
            tracing::info!(target: "rom::mbc1", page_mode=?self.page_mode, "Toggled Page Mode");
            Ok(())
        } else if regions::CARTRIDGE_RAM.contains(loc) {
            if self.ram_enabled {
                let ram_addr = loc.region_offset(regions::CARTRIDGE_RAM);
                if let Some(ram) = ram {
                    ram.write(self.selected_ram_bank(), ram_addr, value)
                }
            }
            Ok(())
        } else {
            unreachable!()
        }
    }

    fn has_battery(&self) -> bool {
        self.has_battery
    }
}

/// MBC2 cartridge controller
pub struct MBC2 {
    selected_rom: u8,
    ram_enabled: bool,
    has_battery: bool,
}

impl MBC2 {
    pub fn new(cartridge_type_id: u8) -> MBC2 {
        MBC2 {
            selected_rom: 1,
            ram_enabled: false,
            has_battery: cartridge_type_id == 6,
        }
    }

    fn selected_rom_bank(&self) -> u8 {
        let bank_id = self.selected_rom & 0xF;
        if bank_id == 0 {
            1
        } else {
            bank_id
        }
    }
}

impl CartridgeController for MBC2 {
    fn read_static_rom(&self, loc: LiteralAddress, rom: &[u8]) -> CartridgeIOResult<u8> {
        Ok(rom[usize::from(loc)])
    }

    fn read_switchable_rom(&self, loc: LiteralAddress, rom: &[u8]) -> CartridgeIOResult<u8> {
        let bank_addr = loc.region_offset(regions::SWITCHABLE_ROM);
        let bank = u16::from(self.selected_rom_bank());
        let rom_addr = (bank * regions::SWITCHABLE_ROM.len()) + bank_addr.0;
        rom.get(usize::from(rom_addr))
            .copied()
            .ok_or(CartridgeIOError::NoDataInRom(loc))
    }

    fn read_switchable_ram(
        &self,
        loc: LiteralAddress,
        ram: &CartridgeRam,
    ) -> CartridgeIOResult<u8> {
        if !self.ram_enabled {
            Err(CartridgeIOError::CartridgeRamDisabled)
        } else {
            let wrapped_ram_addr = loc.region_offset(regions::CARTRIDGE_RAM) % 0x200u16;
            Ok(ram.read(0, wrapped_ram_addr))
        }
    }

    fn write(
        &mut self,
        loc: LiteralAddress,
        value: u8,
        ram: Option<&mut CartridgeRam>,
    ) -> CartridgeIOResult<()> {
        if regions::STATIC_ROM.contains(loc) {
            if loc.0 & 0x100 == 0x100 {
                self.selected_rom = value & 0xF;
                tracing::info!(target: "rom::mbc2", selected_rom_bank = self.selected_rom_bank(), "Selected ROM bank");
            } else {
                self.ram_enabled = (value & 0xF) == 0b1010;
                tracing::info!(target: "rom::mbc2", new_ram_enabled_state = self.ram_enabled, "Toggled RAM");
            }
            Ok(())
        } else if regions::CARTRIDGE_RAM.contains(loc) && self.ram_enabled {
            let ram_addr = loc.region_offset(regions::CARTRIDGE_RAM);
            let wrapped_ram_addr = ram_addr % 0x200u16;
            if let Some(ram) = ram {
                ram.write(0, wrapped_ram_addr, value)
            }
            Ok(())
        } else {
            Ok(())
        }
    }

    fn has_battery(&self) -> bool {
        self.has_battery
    }
}

fn lookup_target(target_id: u8) -> TargetConsole {
    match target_id {
        0xC0 => TargetConsole::ColorOnly,
        0x80 => TargetConsole::ColorEnhanced,
        _ => TargetConsole::GameBoyOnly,
    }
}

pub struct MBC3 {
    selected_rom: u8,
    selected_ram: u8,
    ram_enabled: bool,
    has_ram: bool,
    has_timer: bool,
    has_battery: bool,
}

impl MBC3 {
    pub fn new(cartridge_type_id: u8) -> MBC3 {
        let has_timer = (cartridge_type_id & 0b100) != 0;
        let has_ram = (cartridge_type_id & 0b10) != 0;
        let has_battery = (cartridge_type_id & 0b11) == 0b11;
        MBC3 {
            selected_rom: 1,
            selected_ram: 0,
            ram_enabled: false,
            has_ram,
            has_timer,
            has_battery,
        }
    }

    fn selected_rom_bank(&self) -> u8 {
        self.selected_rom & 0x7F
    }

    fn selected_static_rom_bank(&self) -> u8 {
        0
    }

    fn selected_ram_bank(&self) -> u8 {
        self.selected_ram
    }

    const fn ram_enable_area() -> Range<u16> {
        0x0000..0x2000
    }

    const fn rom_select_area() -> Range<u16> {
        0x2000..0x4000
    }

    const fn ram_select_area() -> Range<u16> {
        0x4000..0x6000
    }

    const fn timer_latch_area() -> Range<u16> {
        0x6000..0x8000
    }
}

impl CartridgeController for MBC3 {
    fn read_static_rom(&self, loc: LiteralAddress, rom: &[u8]) -> CartridgeIOResult<u8> {
        let bank = u32::from(self.selected_static_rom_bank());
        let rom_addr = (bank * u32::from(regions::STATIC_ROM.len())) + u32::from(loc.0);
        rom.get(usize::try_from(rom_addr).expect("ROM too large for host platform"))
            .copied()
            .ok_or(CartridgeIOError::NoDataInRom(loc))
    }

    fn read_switchable_rom(&self, loc: LiteralAddress, rom: &[u8]) -> CartridgeIOResult<u8> {
        let bank_addr = loc.region_offset(regions::SWITCHABLE_ROM);
        let bank = u32::from(self.selected_rom_bank());
        let rom_addr = (bank * u32::from(regions::SWITCHABLE_ROM.len())) + u32::from(bank_addr.0);
        rom.get(usize::try_from(rom_addr).expect("ROM too large for host platform"))
            .copied()
            .ok_or(CartridgeIOError::NoDataInRom(loc))
    }

    fn read_switchable_ram(
        &self,
        loc: LiteralAddress,
        ram: &CartridgeRam,
    ) -> CartridgeIOResult<u8> {
        if self.has_ram && self.ram_enabled {
            let bank = self.selected_ram_bank();
            let ram_addr = loc.region_offset(regions::CARTRIDGE_RAM);
            Ok(ram.read(bank, ram_addr))
        } else if self.has_ram {
            Err(CartridgeIOError::CartridgeRamDisabled)
        } else {
            Err(CartridgeIOError::NoCartridgeRam)
        }
    }

    fn write(
        &mut self,
        loc: LiteralAddress,
        value: u8,
        ram: Option<&mut CartridgeRam>,
    ) -> CartridgeIOResult<()> {
        if MBC3::ram_enable_area().contains(&loc.0) {
            self.ram_enabled = (value & 0xF) == 0b1010;
            tracing::info!(target: "rom::mbc3", new_ram_enabled_state = self.ram_enabled, "Toggled ram");
            Ok(())
        } else if MBC3::rom_select_area().contains(&loc.0) {
            self.selected_rom = value & 0x7F;
            if self.selected_rom == 0 {
                self.selected_rom = 1
            }
            tracing::info!(target: "rom::mbc3", selected_rom_bank = self.selected_rom, "Selected ROM bank");
            Ok(())
        } else if MBC3::ram_select_area().contains(&loc.0) {
            self.selected_ram = value & 0x3;
            tracing::info!(target: "rom::mbc3", selected_ram_bank = self.selected_ram, "Selected RAM bank", );
            Ok(())
        } else if MBC3::timer_latch_area().contains(&loc.0) {
            Ok(())
        } else if regions::CARTRIDGE_RAM.contains(loc) {
            if self.ram_enabled {
                let ram_addr = loc.region_offset(regions::CARTRIDGE_RAM);
                if let Some(r) = ram {
                    r.write(self.selected_ram_bank(), ram_addr, value)
                }
            }
            Ok(())
        } else {
            unreachable!()
        }
    }

    fn has_timer(&self) -> bool {
        self.has_timer
    }

    fn has_battery(&self) -> bool {
        self.has_battery
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const ROM_SIZE_32KIB: u8 = 0x00;
    const ROM_SIZE_128KIB: u8 = 0x02;
    const ROM_SIZE_512KIB: u8 = 0x04;
    const ROM_SIZE_1MIB: u8 = 0x06;

    const RAM_SIZE_ZERO: u8 = 0x00;
    const RAM_SIZE_2KIB_UNOFFICIAL: u8 = 0x01;
    const RAM_SIZE_8KIB: u8 = 0x02;
    const RAM_SIZE_32KIB: u8 = 0x03;
    const RAM_SIZE_128KIB: u8 = 0x04;
    const RAM_SIZE_64KIB: u8 = 0x05;

    #[test]
    fn test_static_rom() {
        let mut rom_data = vec![0x12; 32 * 1024];
        rom_data[CARTRIDGE_TYPE_LOCATION] = 0;
        rom_data[ROM_SIZE_LOCATION] = ROM_SIZE_32KIB;
        rom_data[RAM_SIZE_LOCATION] = RAM_SIZE_ZERO;
        rom_data[0x5500] = 0x23;
        let mut cartridge = Cartridge::from_data(rom_data).unwrap();

        assert_eq!(cartridge.target, TargetConsole::GameBoyOnly);
        assert_eq!(cartridge.read(0x1234.into()).unwrap(), 0x12);
        assert_eq!(cartridge.read(0x5500.into()).unwrap(), 0x23);
        assert_eq!(
            cartridge.read(0xA111.into()),
            Err(CartridgeIOError::NoCartridgeRam)
        );
        assert_eq!(
            cartridge.read(0x9222.into()),
            Err(CartridgeIOError::NonCartAddress(0x9222.into()))
        );
        assert_eq!(cartridge.write(0x1234.into(), 0x22), Ok(()));
        assert_eq!(cartridge.read(0x1234.into()).unwrap(), 0x12);
        assert!(cartridge.ram.is_none());
    }

    #[test]
    fn test_mbc1_large_rom_basic_rom() {
        let mut rom_data = vec![0x12; 128 * 1024];
        rom_data[CARTRIDGE_TYPE_LOCATION] = 1;
        rom_data[RAM_SIZE_LOCATION] = RAM_SIZE_ZERO;
        rom_data[ROM_SIZE_LOCATION] = ROM_SIZE_128KIB;
        rom_data[0x5500] = 0x23;
        let cartridge = Cartridge::from_data(rom_data).unwrap();

        assert_eq!(cartridge.read(0x1234.into()).unwrap(), 0x12);
        assert_eq!(cartridge.read(0x5500.into()).unwrap(), 0x23);
        assert_eq!(
            cartridge.read(0x9222.into()),
            Err(CartridgeIOError::NonCartAddress(0x9222.into()))
        );
        assert_eq!(
            cartridge.read(0xA111.into()),
            Err(CartridgeIOError::NoCartridgeRam)
        );
        assert!(cartridge.ram.is_none());
    }

    #[test]
    fn test_mbc1_large_rom_basic_ram() -> CartridgeIOResult<()> {
        let mut rom_data = vec![0x12; 128 * 1024];
        rom_data[CARTRIDGE_TYPE_LOCATION] = 2;
        rom_data[ROM_SIZE_LOCATION] = ROM_SIZE_128KIB;
        rom_data[RAM_SIZE_LOCATION] = RAM_SIZE_8KIB;
        let mut cartridge = Cartridge::from_data(rom_data).unwrap();

        cartridge.write(0x00ff.into(), 0b1010)?;
        cartridge.write(0xA111.into(), 0x20)?;
        assert_eq!(cartridge.read(0xA111.into())?, 0x20);
        cartridge.write(0x00ff.into(), 0b1000)?;
        cartridge.write(0xA111.into(), 0x20)?;
        assert_eq!(
            cartridge.read(0xA111.into()),
            Err(CartridgeIOError::CartridgeRamDisabled)
        );
        assert!(cartridge.ram.is_some());
        assert_eq!(cartridge.ram.unwrap().size(), 8192);
        Ok(())
    }

    #[test]
    fn test_mbc1_largerom_rom_bank_switch() -> CartridgeIOResult<()> {
        let mut rom_data = vec![0x12; 1024 * 1024];
        rom_data[0x4001] = 0x33;
        rom_data[0x8001] = 0x99;
        rom_data[0x80001] = 0x34;
        rom_data[0x88001] = 0x66;
        rom_data[CARTRIDGE_TYPE_LOCATION] = 1;
        rom_data[ROM_SIZE_LOCATION] = ROM_SIZE_1MIB;
        rom_data[RAM_SIZE_LOCATION] = RAM_SIZE_ZERO;
        let mut cartridge = Cartridge::from_data(rom_data).unwrap();

        assert_eq!(cartridge.read(0x4001.into())?, 0x33, "Default to bank 1");
        cartridge.write(0x2001.into(), 2)?;
        assert_eq!(cartridge.read(0x4001.into())?, 0x99, "Switch to bank 2");
        cartridge.write(0x2001.into(), 0)?;
        assert_eq!(
            cartridge.read(0x4001.into())?,
            0x33,
            "Bank 0 mapped to bank 1"
        );
        cartridge.write(0x2001.into(), 1)?;
        assert_eq!(
            cartridge.read(0x4001.into())?,
            0x33,
            "Bank 1 mapped to bank 1"
        );
        cartridge.write(0x2001.into(), 0x82)?;
        assert_eq!(
            cartridge.read(0x4001.into())?,
            0x99,
            "Only bottom 5 bits of ROM select used to select bank (2)"
        );
        cartridge.write(0x4001.into(), 0x1)?;
        assert_eq!(
            cartridge.read(0x4001.into())?,
            0x66,
            "High select bits used to load ROM > 512 KiB (bank 18)"
        );
        assert_eq!(
            cartridge.read(0x1.into())?,
            0x34,
            "High select bits used to load static ROM (bank 17)"
        );
        Ok(())
    }

    #[test]
    fn test_mbc1_largeram_rom_bank_switch() -> CartridgeIOResult<()> {
        let mut rom_data = vec![0x12; 512 * 1024];
        rom_data[0x4001] = 0x33;
        rom_data[0x8001] = 0x99;
        rom_data[CARTRIDGE_TYPE_LOCATION] = 2;
        rom_data[ROM_SIZE_LOCATION] = ROM_SIZE_512KIB;
        rom_data[RAM_SIZE_LOCATION] = RAM_SIZE_32KIB;
        let mut cartridge = Cartridge::from_data(rom_data).unwrap();
        cartridge.write(0x6001.into(), 1)?;

        assert_eq!(cartridge.read(0x4001.into())?, 0x33, "Default to bank 1");
        cartridge.write(0x2001.into(), 2)?;
        assert_eq!(cartridge.read(0x4001.into())?, 0x99, "Switch to bank 2");
        cartridge.write(0x2001.into(), 0)?;
        assert_eq!(
            cartridge.read(0x4001.into())?,
            0x33,
            "Bank 0 mapped to bank 1"
        );
        cartridge.write(0x2001.into(), 1)?;
        assert_eq!(
            cartridge.read(0x4001.into())?,
            0x33,
            "Bank 1 mapped to bank 1"
        );
        cartridge.write(0x2001.into(), 0x82)?;
        assert_eq!(
            cartridge.read(0x4001.into())?,
            0x99,
            "Only bottom 5 bits of ROM select used to select bank (2)"
        );
        cartridge.write(0x4001.into(), 0x1)?;
        assert_eq!(
            cartridge.read(0x4001.into())?,
            0x99,
            "High select bits not used to load ROM > 512 KiB (bank 18)"
        );
        assert_eq!(
            cartridge.read(0x1.into())?,
            0x12,
            "High select bits not used to load static ROM (bank 17)"
        );
        assert!(cartridge.ram.is_some());
        assert_eq!(cartridge.ram.unwrap().size(), 32 * 1024);
        Ok(())
    }

    #[test]
    fn test_mbc1_largeram_ram_bank_switch() -> CartridgeIOResult<()> {
        let mut rom_data = vec![0x12; 512 * 1024];
        rom_data[CARTRIDGE_TYPE_LOCATION] = 3;
        rom_data[ROM_SIZE_LOCATION] = ROM_SIZE_512KIB;
        rom_data[RAM_SIZE_LOCATION] = RAM_SIZE_32KIB;
        let mut cartridge = Cartridge::from_data(rom_data).unwrap();
        cartridge.write(0x6001.into(), 1)?;
        cartridge.write(0x00ff.into(), 0b1010)?;

        cartridge.write(0xA111.into(), 0x43)?;
        assert_eq!(cartridge.read(0xA111.into()), Ok(0x43));
        cartridge.write(0x4001.into(), 0x1)?;
        assert_ne!(cartridge.read(0xA111.into()), Ok(0x43));
        cartridge.write(0x4001.into(), 0x0)?;
        assert_eq!(cartridge.read(0xA111.into()), Ok(0x43));
        assert!(cartridge.ram.is_some());
        assert_eq!(cartridge.ram.unwrap().size(), 32 * 1024);
        Ok(())
    }

    #[test]
    fn test_mbc1_ram_sizes() {
        let mut rom_data = vec![0x12; 512 * 1024];
        rom_data[CARTRIDGE_TYPE_LOCATION] = 3;
        rom_data[ROM_SIZE_LOCATION] = ROM_SIZE_512KIB;
        rom_data[RAM_SIZE_LOCATION] = RAM_SIZE_2KIB_UNOFFICIAL;
        let cartridge = Cartridge::from_data(rom_data).unwrap();

        assert_eq!(cartridge.ram.unwrap().size(), 2 * 1024);

        let mut rom_data = vec![0x12; 512 * 1024];
        rom_data[CARTRIDGE_TYPE_LOCATION] = 3;
        rom_data[ROM_SIZE_LOCATION] = ROM_SIZE_512KIB;
        rom_data[RAM_SIZE_LOCATION] = RAM_SIZE_128KIB;
        let cartridge = Cartridge::from_data(rom_data).unwrap();

        assert_eq!(cartridge.ram.unwrap().size(), 128 * 1024);

        let mut rom_data = vec![0x12; 512 * 1024];
        rom_data[CARTRIDGE_TYPE_LOCATION] = 3;
        rom_data[ROM_SIZE_LOCATION] = ROM_SIZE_512KIB;
        rom_data[RAM_SIZE_LOCATION] = RAM_SIZE_64KIB;
        let cartridge = Cartridge::from_data(rom_data).unwrap();

        assert_eq!(cartridge.ram.unwrap().size(), 64 * 1024);

        let mut rom_data = vec![0x12; 512 * 1024];
        rom_data[CARTRIDGE_TYPE_LOCATION] = 3;
        rom_data[ROM_SIZE_LOCATION] = ROM_SIZE_512KIB;
        rom_data[RAM_SIZE_LOCATION] = 6;
        let cartridge_result = Cartridge::from_data(rom_data);

        assert_eq!(
            cartridge_result.err().unwrap(),
            CartridgeLoadError::UnsupportedRamSize(6)
        );
    }

    #[test]
    fn test_mbc2_basic_rom() {
        let mut rom_data = vec![0x12; 128 * 1024];
        rom_data[0x5500] = 0x23;
        rom_data[CARTRIDGE_TYPE_LOCATION] = 5;
        rom_data[ROM_SIZE_LOCATION] = ROM_SIZE_128KIB;
        rom_data[RAM_SIZE_LOCATION] = RAM_SIZE_ZERO;
        let mut cartridge = Cartridge::from_data(rom_data).unwrap();

        assert_eq!(cartridge.read(0x1234.into()).unwrap(), 0x12);
        assert_eq!(cartridge.read(0x5500.into()).unwrap(), 0x23);
        assert_eq!(
            cartridge.read(0x9222.into()),
            Err(CartridgeIOError::NonCartAddress(0x9222.into()))
        );
        assert_eq!(
            cartridge.read(0xA111.into()),
            Err(CartridgeIOError::CartridgeRamDisabled)
        );
        assert_eq!(cartridge.write(0x4001.into(), 0x55), Ok(()));
        assert!(cartridge.ram.is_some());
        assert_eq!(cartridge.ram.unwrap().size(), 512);
    }

    #[test]
    fn test_mbc2_basic_ram() -> CartridgeIOResult<()> {
        let mut rom_data = vec![0x12; 128 * 1024];
        rom_data[CARTRIDGE_TYPE_LOCATION] = 6;
        rom_data[ROM_SIZE_LOCATION] = ROM_SIZE_128KIB;
        rom_data[RAM_SIZE_LOCATION] = RAM_SIZE_ZERO;
        let mut cartridge = Cartridge::from_data(rom_data).unwrap();
        cartridge.write(0x00.into(), 0b1010)?;

        cartridge.write(0xA123.into(), 0xF1)?;
        assert_eq!(
            cartridge.read(0xA123.into()),
            Ok(0x1),
            "Bottom nibble only stored"
        );
        assert_eq!(
            cartridge.read(0xA323.into()),
            Ok(0x1),
            "RAM repeats through address space"
        );
        cartridge.write(0x00.into(), 0b1000)?;
        assert_eq!(
            cartridge.read(0xA111.into()),
            Err(CartridgeIOError::CartridgeRamDisabled)
        );
        assert!(cartridge.ram.is_some());
        assert_eq!(cartridge.ram.unwrap().size(), 512);
        Ok(())
    }

    #[test]
    fn test_mbc2_rom_bank_switching() -> CartridgeIOResult<()> {
        let mut rom_data = vec![0x12; 512 * 1024];
        rom_data[0x4001] = 0x33;
        rom_data[0x8001] = 0x99;
        rom_data[CARTRIDGE_TYPE_LOCATION] = 6;
        rom_data[ROM_SIZE_LOCATION] = ROM_SIZE_512KIB;
        rom_data[RAM_SIZE_LOCATION] = RAM_SIZE_ZERO;
        let mut cartridge = Cartridge::from_data(rom_data).unwrap();

        assert_eq!(cartridge.read(0x4001.into()).unwrap(), 0x33);
        cartridge.write(0x100.into(), 0b10)?;
        assert_eq!(cartridge.read(0x4001.into()).unwrap(), 0x99);
        Ok(())
    }

    #[test]
    fn test_target_detection() {
        let mut rom_data = vec![0x12; 512 * 1024];
        rom_data[RAM_SIZE_LOCATION] = RAM_SIZE_ZERO;
        rom_data[ROM_SIZE_LOCATION] = ROM_SIZE_512KIB;
        rom_data[CARTRIDGE_TYPE_LOCATION] = 6;
        let cartridge = Cartridge::from_data(rom_data.clone()).unwrap();

        assert_eq!(cartridge.target, TargetConsole::GameBoyOnly);

        rom_data[TARGET_CONSOLE_LOCATION] = 0xC0;
        let cartridge = Cartridge::from_data(rom_data.clone()).unwrap();

        assert_eq!(cartridge.target, TargetConsole::ColorOnly);

        rom_data[TARGET_CONSOLE_LOCATION] = 0x80;
        let cartridge = Cartridge::from_data(rom_data.clone()).unwrap();

        assert_eq!(cartridge.target, TargetConsole::ColorEnhanced);
    }
}
