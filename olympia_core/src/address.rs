//! Represents a variety of addressing types for
//! emulation.

use core::{
    convert::{TryFrom, TryInto},
    ops::{Add, Rem, Sub},
};

use derive_more::{
    derive::{Error, UpperHex},
    Display, From, FromStr, Into,
};

#[derive(
    PartialEq, Eq, PartialOrd, Ord, Debug, Copy, Clone, From, FromStr, Into, Display, UpperHex,
)]
/// Represents a literal memory address
#[display("[{:X}h]", _0)]
pub struct LiteralAddress(pub u16);

impl From<LiteralAddress> for usize {
    fn from(addr: LiteralAddress) -> usize {
        addr.0.into()
    }
}

impl<T: Into<u16>> Add<T> for LiteralAddress {
    type Output = LiteralAddress;

    fn add(self, rhs: T) -> Self::Output {
        LiteralAddress(self.0 + rhs.into())
    }
}

impl<T: Into<u16>> Sub<T> for LiteralAddress {
    type Output = LiteralAddress;

    fn sub(self, rhs: T) -> Self::Output {
        LiteralAddress(self.0 - rhs.into())
    }
}

#[derive(PartialEq, Eq, Debug, Clone, Display, Error, From)]
pub enum OffsetConversionError {
    #[display("Address 0x{_0:X} not in range {_1}")]
    OutOfRange(#[error(not(source))] LiteralAddress, MemoryRegion),
    Unmapped(NeedRangeMapping),
}

impl LiteralAddress {
    /// Get the address immediately following this one, wrapping if needed
    pub fn next(self) -> LiteralAddress {
        LiteralAddress(self.0.wrapping_add(1))
    }

    pub fn region_offset(self, region: MemoryRegion) -> BankOffsetAddress {
        BankOffsetAddress(self.0 - region.start)
    }

    pub fn bank_offset(self, bank: Bank) -> Result<BankOffsetAddress, OffsetConversionError> {
        let region: MemoryRegion = bank.try_into()?;
        if !region.contains(self) {
            Err(OffsetConversionError::OutOfRange(self, region))
        } else {
            Ok(self.region_offset(region))
        }
    }

    pub fn saturating_add(self, rhs: u16) -> LiteralAddress {
        LiteralAddress(self.0.saturating_add(rhs))
    }
}

impl From<[u8; 2]> for LiteralAddress {
    fn from(bytes: [u8; 2]) -> Self {
        LiteralAddress(u16::from_le_bytes(bytes))
    }
}

impl From<HighAddress> for LiteralAddress {
    fn from(addr: HighAddress) -> Self {
        LiteralAddress(u16::from(addr.0) + 0xff00)
    }
}

/// Represents an address defined as the start of its bank
#[derive(PartialEq, Eq, Debug, Copy, Clone, From, FromStr, Into, Display, UpperHex)]
#[display("[{:X}h]", _0)]
pub struct BankOffsetAddress(pub u16);

impl From<BankOffsetAddress> for usize {
    fn from(address: BankOffsetAddress) -> usize {
        address.0.into()
    }
}

impl<T: Into<u16>> Add<T> for BankOffsetAddress {
    type Output = BankOffsetAddress;

    fn add(self, rhs: T) -> Self::Output {
        BankOffsetAddress(self.0 + rhs.into())
    }
}

impl<T: Into<u16>> Sub<T> for BankOffsetAddress {
    type Output = BankOffsetAddress;

    fn sub(self, rhs: T) -> Self::Output {
        BankOffsetAddress(self.0 - rhs.into())
    }
}

impl<T: Into<u16>> Rem<T> for BankOffsetAddress {
    type Output = BankOffsetAddress;

    fn rem(self, rhs: T) -> Self::Output {
        BankOffsetAddress(self.0 % rhs.into())
    }
}

/// Represents a bank of ROM or RAM controlled by the cartridge
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum CartridgeBank {
    Rom(u16),
    Ram(u8),
}

/// Represents a region of memory within the gameboy's memory map
#[derive(PartialEq, Eq, Debug, Clone, Copy, Display)]
#[display("{} [{} {}]", self.name, self.start, self.last())]
pub struct MemoryRegion {
    start: u16,
    len: u16,
    name: &'static str,
}

#[allow(clippy::len_without_is_empty)]
impl MemoryRegion {
    const fn new(start: u16, len: u16, name: &'static str) -> MemoryRegion {
        MemoryRegion { start, len, name }
    }

    /// Check if an address is within a given memory region
    pub fn contains(&self, addr: LiteralAddress) -> bool {
        addr.0 >= self.start && addr.0 <= self.last().0
    }

    pub fn first(&self) -> LiteralAddress {
        LiteralAddress(self.start)
    }

    /// Get the last valid address in the memory region
    pub fn last(&self) -> LiteralAddress {
        LiteralAddress((self.start + self.len) - 1)
    }

    /// Get the size of the memory region
    pub fn len(&self) -> u16 {
        self.len
    }

    pub fn name(&self) -> &'static str {
        self.name
    }
}

pub mod regions {
    use super::*;
    pub const STATIC_ROM: MemoryRegion = MemoryRegion::new(0x0000, 0x4000, "staticrom");
    pub const SWITCHABLE_ROM: MemoryRegion = MemoryRegion::new(0x4000, 0x4000, "switchrom");
    pub const CARTRIDGE_ROM: MemoryRegion = MemoryRegion::new(0x0000, 0x4000, "rom");
    pub const VRAM: MemoryRegion = MemoryRegion::new(0x8000, 0x2000, "vram");
    pub const CARTRIDGE_RAM: MemoryRegion = MemoryRegion::new(0xA000, 0x2000, "cartram");
    pub const SYS_RAM: MemoryRegion = MemoryRegion::new(0xC000, 0x2000, "sysram");
    pub const SYS_RAM_MIRROR: MemoryRegion = MemoryRegion::new(0xE000, 0x1E00, "sysram_mirror");
    pub const OAM_RAM: MemoryRegion = MemoryRegion::new(0xFE00, 0xA0, "oamram");
    pub const MEM_REGISTERS: MemoryRegion = MemoryRegion::new(0xFF00, 0x80, "memregisters");
    pub const CPU_RAM: MemoryRegion = MemoryRegion::new(0xFF80, 0x7F, "cpuram");
    pub const MODEL_RESERVED: MemoryRegion = MemoryRegion::new(0xFEA0, 0x60, "modelreserved");
}

/// A potential bank of memory for use in banked addresses
///
/// Note: Different banks can overlap depending on the current
/// memory mapping state
#[derive(PartialEq, Eq, Debug, Clone, Copy, Display)]
pub enum Bank {
    /// A bank of (potentially not currently mapped) cartridge RAM or RAM
    #[display("_0")]
    Cartridge(CartridgeBank),
    /// Base ROM section of a cartridge (0x0000 -> 0x3FFF)
    #[display("ROM0")]
    Rom0,
    /// Currently mapped mappable ROM section of a cartridge (0x4000 -> 0x7FFF)
    #[display("ROMX")]
    RomX,
    /// Video RAM (0x8000 -> 0x9FFF)
    #[display("VRAM")]
    Vram,
    /// The currently mapped external RAM (0xA000 -> 0xBFFF)
    ///
    /// Can overlap with Cartridge::Ram sections if those sections are
    /// currently mapped into the gameboy's memory
    #[display("SRAM")]
    Sram,
    /// System RAM, aka WRAM (0xC000 -> 0xDFFF)
    #[display("WRAM")]
    SysRam,
    /// Object (Sprite) memory (0xFE00 -> )
    #[display("OAM")]
    Oam,
    /// High (CPU) RAM (0xFF80 -> 0xFFFE)
    #[display("HRAM")]
    Hram,
}

impl Bank {
    /// Reference a specific bank of cartridge based ROM
    pub fn cartridge_rom(bank: u16) -> Bank {
        Bank::Cartridge(CartridgeBank::Rom(bank))
    }

    /// Reference a specific bank of cartridge based RAM.
    pub fn cartridge_ram(bank: u8) -> Bank {
        Bank::Cartridge(CartridgeBank::Ram(bank))
    }
}

/// The address of this region can only be determined by looking
/// at the current memory map
#[derive(Debug, PartialEq, Eq, Clone, Copy, Display, Error)]
#[display("Cannot convert bank to memory region without memory mapping")]
pub struct NeedRangeMapping;

impl TryFrom<Bank> for MemoryRegion {
    type Error = NeedRangeMapping;

    fn try_from(value: Bank) -> Result<Self, Self::Error> {
        match value {
            Bank::Cartridge(CartridgeBank::Rom(0)) => Ok(regions::STATIC_ROM),
            Bank::Cartridge(_) => Err(NeedRangeMapping),
            Bank::Rom0 => Ok(regions::STATIC_ROM),
            Bank::RomX => Ok(regions::SWITCHABLE_ROM),
            Bank::Vram => Ok(regions::VRAM),
            Bank::Sram => Ok(regions::CARTRIDGE_RAM),
            Bank::SysRam => Ok(regions::SYS_RAM),
            Bank::Oam => Ok(regions::OAM_RAM),
            Bank::Hram => Ok(regions::CPU_RAM),
        }
    }
}

#[derive(PartialEq, Eq, Debug, Copy, Clone, From, Into, Display)]
#[display("{bank}:{offset}")]
pub struct BankedAddress {
    bank: Bank,
    offset: BankOffsetAddress,
}

impl TryFrom<BankedAddress> for LiteralAddress {
    type Error = NeedRangeMapping;

    fn try_from(value: BankedAddress) -> Result<Self, Self::Error> {
        let region: MemoryRegion = value.bank.try_into()?;
        assert!(
            value.offset.0 < region.len(),
            "Invalid banked address, value {} out of range for region {}",
            value.offset.0,
            region.name
        );
        Ok(LiteralAddress(region.start + value.offset.0))
    }
}

#[derive(PartialEq, Eq, Debug, Clone, Copy, Display, Error)]
pub enum InvalidBankedAddress {
    #[display("Address {:X} out of range for bank {:?}", _1, _0)]
    AddressOutOfRange(Bank, BankOffsetAddress),
}

impl BankedAddress {
    pub fn new(
        bank: Bank,
        offset: BankOffsetAddress,
    ) -> Result<BankedAddress, InvalidBankedAddress> {
        let address_limit = match bank {
            Bank::Cartridge(CartridgeBank::Rom(_)) => 0x4000,
            Bank::Cartridge(CartridgeBank::Ram(_)) => 0x2000,
            _ => MemoryRegion::try_from(bank)
                .expect("Memory region missing defined type")
                .len(),
        };
        if offset.0 < address_limit {
            Ok(BankedAddress { bank, offset })
        } else {
            Err(InvalidBankedAddress::AddressOutOfRange(bank, offset))
        }
    }

    pub fn bank(&self) -> Bank {
        self.bank
    }

    pub fn offset(&self) -> BankOffsetAddress {
        self.offset
    }
}

#[derive(PartialEq, Eq, Debug, Copy, Clone, From, Into)]
/// Represents an address in high memory (offset from 0xFF00)
pub struct HighAddress(pub u8);

#[derive(PartialEq, Eq, Debug, Copy, Clone)]
#[doc(hidden)]
pub struct OffsetResolveResult {
    pub addr: LiteralAddress,
    pub half_carry: bool,
    pub carry: bool,
}

#[derive(PartialEq, Eq, Debug, Copy, Clone)]
/// Represents an address that is offset from the program counter
pub struct AddressOffset(pub i8);

impl From<u8> for AddressOffset {
    fn from(addr: u8) -> Self {
        AddressOffset(i8::from_le_bytes([addr]))
    }
}

impl From<AddressOffset> for u8 {
    fn from(addr: AddressOffset) -> Self {
        addr.0 as u8
    }
}

impl AddressOffset {
    /// Add offset to a given base to find a new address
    ///
    /// base is the address to offset from, which in the
    /// gameboy instruction set is based on the PC or SP
    /// register, depending on the instruction
    pub fn resolve(self, base: LiteralAddress) -> LiteralAddress {
        self.resolve_internal(base).addr
    }

    /// Returns new address, half carry and carry flags
    ///
    /// base is the address to offset from, which in the
    /// gameboy instruction set is based on the PC or SP
    /// register, depending on the instruction
    #[doc(hidden)]
    #[allow(clippy::branches_sharing_code)]
    pub fn resolve_internal(self, base: LiteralAddress) -> OffsetResolveResult {
        use core::convert::TryFrom;
        let raw_base = base.0;
        let offset = self.0;
        let (new_addr, half_carry, carry) = if offset < 0 {
            let to_sub = u16::try_from(offset.abs()).unwrap();
            let (new, carry) = raw_base.overflowing_sub(to_sub);
            let half_carry = ((raw_base & 0xF) + 0x10) - (to_sub & 0xF) < 0x10;
            (new, half_carry, carry)
        } else {
            let to_add = u16::try_from(offset.abs()).unwrap();
            let half_add = ((raw_base & 0xF) + (to_add & 0xF)) & 0xF0;
            let (new, carry) = raw_base.overflowing_add(to_add);
            (new, half_add != 0, carry)
        };
        OffsetResolveResult {
            addr: new_addr.into(),
            half_carry,
            carry,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_convert_bytes_to_address() {
        assert_eq!(LiteralAddress::from([0x54, 0x32]), LiteralAddress(0x3254));
    }

    #[test]
    fn test_resolve_address_postive_offset() {
        let positive_offset = AddressOffset(0x2C);

        assert_eq!(
            positive_offset.resolve_internal(0x1000.into()),
            OffsetResolveResult {
                addr: 0x102C.into(),
                carry: false,
                half_carry: false,
            }
        );

        assert_eq!(
            positive_offset.resolve(0x1000.into()),
            LiteralAddress(0x102C),
        );

        assert_eq!(
            positive_offset.resolve_internal(0x1004.into()),
            OffsetResolveResult {
                addr: 0x1030.into(),
                carry: false,
                half_carry: true,
            }
        );

        assert_eq!(
            positive_offset.resolve_internal(0xFFF0.into()),
            OffsetResolveResult {
                addr: 0x001C.into(),
                carry: true,
                half_carry: false,
            }
        );

        assert_eq!(
            positive_offset.resolve_internal(0xFFFF.into()),
            OffsetResolveResult {
                addr: 0x002B.into(),
                carry: true,
                half_carry: true,
            }
        );
    }

    #[test]
    fn test_resolve_address_negative_offset() {
        let positive_offset = AddressOffset(-0x19);

        assert_eq!(
            positive_offset.resolve_internal(0x102C.into()),
            OffsetResolveResult {
                addr: 0x1013.into(),
                carry: false,
                half_carry: false,
            }
        );

        assert_eq!(
            positive_offset.resolve_internal(0x1004.into()),
            OffsetResolveResult {
                addr: 0x0FEB.into(),
                carry: false,
                half_carry: true,
            }
        );

        assert_eq!(
            positive_offset.resolve_internal(0x000A.into()),
            OffsetResolveResult {
                addr: 0xFFF1.into(),
                carry: true,
                half_carry: false,
            }
        );

        assert_eq!(
            positive_offset.resolve_internal(0x0000.into()),
            OffsetResolveResult {
                addr: 0xFFE7.into(),
                carry: true,
                half_carry: true,
            }
        );
    }
}
