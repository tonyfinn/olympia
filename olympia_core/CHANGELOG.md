# Changelog

## 0.4.0 [Unreleased]

### Breaking changes

* AddressOffset.resolve() does now returns a LiteralAddress directly
* Requires Rust 1.81.0

## 0.3.0

* Add some convenience methods for use in frontends
* Sync version with olympia_engine

## 0.1.0

* Initial implementation