# ROMs included in this test suite

* 2048
  - [2048.gb](https://github.com/Sanqui/2048-gb) is 2048-gb by Sanqui. It is available under the zlib license. See [2048-gb.txt](./2048-gb.txt) for full license info.
* fizzbuzz
  - For 0..100, stores in high memory 0xFB if divisible by 15, 0xF0 if divisible by 
    3 or 0xB0 if divisble by 5. Otherwise just stores the number itself. Used for CPU
    testing.
* olympia
  - Renders the word "olympia" using background sprites
