DEF rJOYP EQU $ff00 ; Joypad (R/W)
DEF rSB EQU $ff01 ; Serial transfer data (R/W)
DEF rSC EQU $ff02 ; Serial Transfer Control (R/W)
DEF rDIV EQU $ff04 ; Divider Register (R/W)
DEF rTIMA EQU $ff05 ; Timer counter (R/W)
DEF rTMA EQU $ff06 ; Timer Modulo (R/W)
DEF rTAC EQU $ff07 ; Timer Control (R/W)
DEF rIF EQU $ff0f ; Interrupt Flag (R/W)
DEF rNR10 EQU $ff10 ; Channel 1 Sweep register (R/W)
DEF rNR11 EQU $ff11 ; Channel 1 Sound length/Wave pattern duty (R/W)
DEF rNR12 EQU $ff12 ; Channel 1 Volume Envelope (R/W)
DEF rNR13 EQU $ff13 ; Channel 1 Frequency lo (Write Only)
DEF rNR14 EQU $ff14 ; Channel 1 Frequency hi (R/W)
DEF rNR21 EQU $ff16 ; Channel 2 Sound Length/Wave Pattern Duty (R/W)
DEF rNR22 EQU $ff17 ; Channel 2 Volume Envelope (R/W)
DEF rNR23 EQU $ff18 ; Channel 2 Frequency lo data (W)
DEF rNR24 EQU $ff19 ; Channel 2 Frequency hi data (R/W)
DEF rNR30 EQU $ff1a ; Channel 3 Sound on/off (R/W)
DEF rNR31 EQU $ff1b ; Channel 3 Sound Length
DEF rNR32 EQU $ff1c ; Channel 3 Select output level (R/W)
DEF rNR33 EQU $ff1d ; Channel 3 Frequency's lower data (W)
DEF rNR34 EQU $ff1e ; Channel 3 Frequency's higher data (R/W)
DEF rNR41 EQU $ff20 ; Channel 4 Sound Length (R/W)
DEF rNR42 EQU $ff21 ; Channel 4 Volume Envelope (R/W)
DEF rNR43 EQU $ff22 ; Channel 4 Polynomial Counter (R/W)
DEF rNR44 EQU $ff23 ; Channel 4 Counter/consecutive; Inital (R/W)
DEF rNR50 EQU $ff24 ; Channel control / ON-OFF / Volume (R/W)
DEF rNR51 EQU $ff25 ; Selection of Sound output terminal (R/W)
DEF rNR52 EQU $ff26 ; Sound on/off
DEF rLCDC EQU $ff40 ; LCD Control (R/W)
DEF rSTAT EQU $ff41 ; LCDC Status (R/W)
DEF rSCY EQU $ff42 ; Scroll Y (R/W)
DEF rSCX EQU $ff43 ; Scroll X (R/W)
DEF rLY EQU $ff44 ; LCDC Y-Coordinate (R)
DEF rLYC EQU $ff45 ; LY Compare (R/W)
DEF rDMA EQU $ff46 ; DMA Transfer and Start Address (W)
DEF rBGP EQU $ff47 ; BG Palette Data (R/W) - Non CGB Mode Only
DEF rOBP0 EQU $ff48 ; Object Palette 0 Data (R/W) - Non CGB Mode Only
DEF rOBP1 EQU $ff49 ; Object Palette 1 Data (R/W) - Non CGB Mode Only
DEF rWY EQU $ff4a ; Window Y Position (R/W)
DEF rWX EQU $ff4b ; Window X Position minus 7 (R/W)
DEF rKEY1 EQU $ff4d ; CGB Mode Only - Prepare Speed Switch
DEF rVBK EQU $ff4f ; CGB Mode Only - VRAM Bank
DEF rHDMA1 EQU $ff51 ; CGB Mode Only - New DMA Source, High
DEF rHDMA2 EQU $ff52 ; CGB Mode Only - New DMA Source, Low
DEF rHDMA3 EQU $ff53 ; CGB Mode Only - New DMA Destination, High
DEF rHDMA4 EQU $ff54 ; CGB Mode Only - New DMA Destination, Low
DEF rHDMA5 EQU $ff55 ; CGB Mode Only - New DMA Length/Mode/Start
DEF rRP EQU $ff56 ; CGB Mode Only - Infrared Communications Port
DEF rBGPI EQU $ff68 ; CGB Mode Only - Background Palette Index
DEF rBGPD EQU $ff69 ; CGB Mode Only - Background Palette Data
DEF rOBPI EQU $ff6a ; CGB Mode Only - Sprite Palette Index
DEF rOBPD EQU $ff6b ; CGB Mode Only - Sprite Palette Data
DEF rUNKNOWN1 EQU $ff6c ; (FEh) Bit 0 (Read/Write) - CGB Mode Only
DEF rSVBK EQU $ff70 ; CGB Mode Only - WRAM Bank
DEF rUNKNOWN2 EQU $ff72 ; (00h) - Bit 0-7 (Read/Write)
DEF rUNKNOWN3 EQU $ff73 ; (00h) - Bit 0-7 (Read/Write)
DEF rUNKNOWN4 EQU $ff74 ; (00h) - Bit 0-7 (Read/Write) - CGB Mode Only
DEF rUNKNOWN5 EQU $ff75 ; (8Fh) - Bit 4-6 (Read/Write)
DEF rUNKNOWN6 EQU $ff76 ; (00h) - Always 00h (Read Only)
DEF rUNKNOWN7 EQU $ff77 ; (00h) - Always 00h (Read Only)
DEF rIE EQU $ffff ; Interrupt Enable (R/W)

; MBC1
DEF MBC1SRamEnable      EQU $0000
DEF MBC1RomBank         EQU $2000
DEF MBC1SRamBank        EQU $4000
DEF MBC1SRamBankingMode EQU $6000

DEF SRAM_DISABLE EQU $00
DEF SRAM_ENABLE  EQU $0a

; macro for easily selecting coordinates on screen
DEF W_TILEMAP EQU $C000
DEF W_OAM EQU $C200

;\1 = X
;\2 = Y
MACRO FuncCoord
    DEF Coord = W_TILEMAP + 20 * \2 + \1
	ENDM
	
MACRO decoord
    FuncCoord \2, \1
    ld de, Coord
    ENDM

MACRO hlcoord
    FuncCoord \2, \1
    ld hl, Coord
    ENDM

; fast copy macros
MACRO waithblank
.wait\@
    ld a, [rSTAT]
    and %00000011
    and a
    jr nz, .wait\@
    ENDM
    
MACRO popdetohli
rept \1
    pop de
    ld a, e
    ld [hli], a
    ld a, d
    ld [hli], a
endr
    ENDM

; convenience
MACRO lda
    ld a, \2
    ld \1, a
    ENDM
MACRO ld0
    xor a
    ld \1, a
    ENDM
    
MACRO djnz
    dec b
    jr nz, \1
    ENDM

; game stuff

DEF W_2048GRID EQU $C800 ; MUST be aligned to 00
DEF W_ANIMFRAME EQU $C880
DEF W_ANIMFRAMES EQU $C900

DEF W_ANIMSCORETILES EQU $CFF0
DEF W_SPTMP EQU $CFFE

; Other variables

DEF H_VCOPY_D EQU $FF90
DEF H_VCOPY_E EQU $FF91
DEF H_VCOPY_H EQU $FF92
DEF H_VCOPY_L EQU $FF93
DEF H_VCOPY_TIMES EQU $FF94
DEF H_VCOPY_ROWS EQU $FF95
DEF H_FAST_VCOPY EQU $FF96
DEF H_FIRSTVBLANK EQU $FF97
;DEF H_FAST_PART EQU $FF97

DEF H_CURDIR EQU $FFA0
DEF H_CURADD EQU $FFA1
DEF H_CURNEXT EQU $FFA2
DEF H_CURANIMFRAME EQU $FFA3
DEF H_CURTMAPADD EQU $FFA4 ; 2 bytes

DEF H_ANIMATE EQU $FFA6
DEF H_ANIMSUB EQU $FFA7
DEF H_ANIMFRAME EQU $FFA8
DEF H_ANIMDIR EQU $FFA9
DEF H_ANIMSCORE EQU $FFAA

DEF H_SCORE EQU $FFB0
DEF H_HIGHSCORE EQU $FFB3
DEF H_PLUSSCORE EQU $FFB6
DEF H_GAMEOVER EQU $FFB8
DEF H_CONTINUING EQU $FFB9
DEF H_GAMESBEATEN EQU $FFBA ; 2 bytes

DEF H_TIMER EQU $FFF0
DEF H_RNG1 EQU $FFF1
DEF H_JOY EQU $fff8
DEF H_JOYOLD EQU $fff9
DEF H_JOYNEW EQU $fffA
