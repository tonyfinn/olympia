mod builder;
mod emulator;
mod screens;
mod utils;
mod widgets;

use gtk4::gio;
use gtk4::prelude::*;
use gtk4::Application;
use gtk4::Builder;
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

struct EmulatorApp {
    gtk_app: Application,
}

impl EmulatorApp {
    fn new() -> EmulatorApp {
        let gtk_app = Application::builder()
            .application_id("com.tonyfinn.olympia_native")
            .build();

        let mut emu = EmulatorApp { gtk_app };
        emu.register_events();
        emu
    }

    fn register_events(&mut self) {
        self.gtk_app.connect_startup(|app| {
            let quit = gio::SimpleAction::new("quit", None);
            quit.connect_activate(|_, _| std::process::exit(0));
            app.add_action(&quit);

            let menu_builder = Builder::from_string(include_str!("../res/menu.ui"));
            let app_main_menu: gio::Menu = menu_builder.object("MainMenu").unwrap();
            app.set_menubar(Some(&app_main_menu));
        });

        self.gtk_app.connect_activate(|app| {
            let debugger_window = screens::Debugger::new(app);
            debugger_window.show_all();
        });
    }

    fn start(self) {
        self.gtk_app.run();
    }
}

fn main() {
    tracing_subscriber::registry()
        .with(fmt::layer())
        .with(EnvFilter::from_env("OLYMPIA_LOG"))
        .init();
    let app = EmulatorApp::new();
    widgets::register();
    app.start();
}
