use crate::utils::EmulatorHandle;
use crate::widgets::common::emu_param_spec;

use gtk4::glib::subclass::InitializingObject;
use gtk4::subclass::prelude::*;
use gtk4::{glib, glib::prelude::*, prelude::*, CompositeTemplate};
use olympia_engine::{
    address::regions::VRAM,
    gameboy::{GBPixel, Palette},
};
use std::cell::RefCell;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{Arc, LazyLock};

use super::common::{EmulatorWidget, EMU_PROPERTY};
use super::emulator_display::GBDisplayBuffer;

pub const SPRITES_PER_LINE: usize = 16;
pub const TOTAL_SPRITES: usize = 384;
pub const SPRITE_ROW_COUNT: usize = TOTAL_SPRITES / SPRITES_PER_LINE;
pub const SPRITE_WIDTH: usize = 8;
pub const SPRITE_SCALE: usize = 2;

#[derive(CompositeTemplate)]
#[template(file = "../../res/tileset_viewer.ui")]
pub struct TilesetViewerInternal {
    #[template_child]
    large_sprites_check: TemplateChild<gtk4::CheckButton>,
    #[template_child]
    drawing_area: TemplateChild<gtk4::DrawingArea>,
    #[template_child]
    refresh_button: TemplateChild<gtk4::Button>,
    buffer: RefCell<GBDisplayBuffer>,
    large_sprites_enabled: AtomicBool,
    emu: RefCell<Option<EmulatorHandle>>,
}

#[glib::object_subclass]
impl ObjectSubclass for TilesetViewerInternal {
    const NAME: &'static str = "OlympiaTilesetViewer";
    type ParentType = gtk4::Box;
    type Type = TilesetViewer;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
    }

    fn instance_init(obj: &InitializingObject<Self>) {
        obj.init_template();
    }
}

impl Default for TilesetViewerInternal {
    fn default() -> TilesetViewerInternal {
        TilesetViewerInternal {
            buffer: RefCell::new(GBDisplayBuffer::new(
                SPRITES_PER_LINE * SPRITE_WIDTH,
                (TOTAL_SPRITES / SPRITES_PER_LINE) * SPRITE_WIDTH,
                SPRITE_SCALE,
            )),
            large_sprites_check: Default::default(),
            drawing_area: Default::default(),
            emu: Default::default(),
            large_sprites_enabled: Default::default(),
            refresh_button: Default::default(),
        }
    }
}

const LARGE_SPRITES_PROPERTY: &str = "large-sprites";

impl ObjectImpl for TilesetViewerInternal {
    fn constructed(&self) {
        self.parent_constructed();

        self.drawing_area
            .set_content_width((SPRITE_SCALE * SPRITE_WIDTH * SPRITES_PER_LINE) as i32);
        self.drawing_area
            .set_content_height((SPRITE_SCALE * SPRITE_WIDTH * SPRITE_ROW_COUNT) as i32);
        self.drawing_area.set_draw_func(glib::clone!(
            #[to_owned(rename_to = imp)]
            self,
            move |_drawing_area, cr, _, _| {
                let buffer = imp.buffer.borrow();
                buffer.render_to_context(cr);
            }
        ));

        let obj = self.obj();
        self.refresh_button.connect_clicked(glib::clone!(
            #[strong]
            obj,
            move |_| {
                obj.render();
            }
        ));
    }

    fn properties() -> &'static [glib::ParamSpec] {
        static PROPERTIES: LazyLock<Arc<Vec<glib::ParamSpec>>> = LazyLock::new(|| {
            Arc::new(vec![
                emu_param_spec(),
                glib::ParamSpecBoolean::builder(LARGE_SPRITES_PROPERTY)
                    .flags(glib::ParamFlags::READWRITE)
                    .build(),
            ])
        });
        PROPERTIES.as_ref()
    }

    fn set_property(&self, _id: usize, value: &glib::Value, pspec: &glib::ParamSpec) {
        match pspec.name() {
            EMU_PROPERTY => {
                self.emu.replace(Some(value.get().unwrap()));
            }
            LARGE_SPRITES_PROPERTY => {
                self.large_sprites_enabled
                    .store(value.get().unwrap(), Ordering::Relaxed);
            }
            _ => unimplemented!(),
        }
    }

    // Called whenever a property is retrieved from this instance. The id
    // is the same as the index of the property in the PROPERTIES array.
    fn property(&self, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
        match pspec.name() {
            EMU_PROPERTY => match self.emu.borrow().as_ref() {
                Some(emu) => emu.clone().to_value(),
                None => panic!("No connected emulator"),
            },
            LARGE_SPRITES_PROPERTY => self
                .large_sprites_enabled
                .load(Ordering::Relaxed)
                .to_value(),
            _ => unimplemented!(),
        }
    }
}

impl WidgetImpl for TilesetViewerInternal {}

impl BoxImpl for TilesetViewerInternal {}

glib::wrapper! {
    pub struct TilesetViewer(ObjectSubclass<TilesetViewerInternal>)
        @extends gtk4::Box, gtk4::Widget,
        @implements gtk4::Buildable, gtk4::Orientable;
}

impl TilesetViewer {
    pub fn render(&self) {
        glib::MainContext::ref_thread_default().spawn_local(self.clone().render_internal());
    }

    pub async fn render_internal(self) {
        let internal = TilesetViewerInternal::from_obj(&self);
        let emu = match internal.emu.borrow().clone() {
            Some(emu) => emu,
            None => return,
        };

        let mem = match emu.query_memory(VRAM.first(), VRAM.last()).await {
            Ok(mem) => mem,
            Err(_) => return,
        };
        for i in 0..TOTAL_SPRITES {
            self.render_sprite(i, &mem.data, &mut internal.buffer.borrow_mut());
        }
        internal.buffer.borrow_mut().swap_buffers();
        internal.drawing_area.queue_draw();
    }

    pub(crate) fn render_sprite(
        &self,
        index: usize,
        data: &[Option<u8>],
        buffer: &mut GBDisplayBuffer,
    ) {
        let sprite_base_x = (index % SPRITES_PER_LINE) * SPRITE_WIDTH;
        let sprite_base_y = (index / SPRITES_PER_LINE) * SPRITE_WIDTH;
        for x in 0..SPRITE_WIDTH {
            for y in 0..SPRITE_WIDTH {
                let palette_index = Self::read_pixel_palette_index(index, data, x, y);
                let pixel = GBPixel::new(Palette::Background, palette_index);
                buffer.draw_pixel(sprite_base_x + x, sprite_base_y + y, &pixel);
            }
        }
    }

    pub fn read_pixel_palette_index(
        tile_index: usize,
        data: &[Option<u8>],
        x: usize,
        y: usize,
    ) -> u8 {
        let lower_addr = (tile_index * 0x10) + (y * 2);

        let lower_byte = data.get(lower_addr).copied().flatten().unwrap_or(0);
        let upper_byte = data.get(lower_addr + 1).copied().flatten().unwrap_or(0);

        let upper_byte_value = (upper_byte >> (7 - x)) & 1;
        let lower_byte_value = (lower_byte >> (7 - x)) & 1;

        lower_byte_value | (upper_byte_value << 1)
    }
}

impl EmulatorWidget for TilesetViewer {}
