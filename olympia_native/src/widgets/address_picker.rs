use olympia_engine::{monitor::parse_number, registers::WordRegister};

use gtk4::glib::{
    self, clone,
    prelude::*,
    subclass::{prelude::*, InitializingObject, Signal},
    wrapper as glib_wrapper, ParamFlags, ParamSpec, ParamSpecString,
};

use gtk4::prelude::*;
use gtk4::subclass::prelude::*;
use gtk4::CompositeTemplate;
use std::{
    cell::RefCell,
    sync::{
        atomic::{AtomicU16, Ordering},
        LazyLock,
    },
};

use crate::{
    utils::{EmulatorHandle, GValueExt},
    widgets::common::{emu_param_spec, EMU_PROPERTY},
};

use super::common::EmulatorWidget;

#[derive(CompositeTemplate, Default)]
#[template(file = "../../res/address_picker.ui")]
pub struct AddressPickerInternal {
    #[template_child]
    pub(crate) address_entry: TemplateChild<gtk4::Entry>,
    #[template_child]
    pub(crate) pc_button: TemplateChild<gtk4::Button>,
    #[template_child]
    pub(crate) go_button: TemplateChild<gtk4::Button>,
    emu: RefCell<Option<EmulatorHandle>>,
    address_selected: AtomicU16,
}

#[glib::object_subclass]
impl ObjectSubclass for AddressPickerInternal {
    const NAME: &'static str = "OlympiaAddressPicker";
    type ParentType = gtk4::Box;
    type Type = AddressPicker;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
    }

    fn instance_init(obj: &InitializingObject<Self>) {
        obj.init_template();
    }
}

glib_wrapper! {
    pub struct AddressPicker(ObjectSubclass<AddressPickerInternal>)
        @extends gtk4::Box, gtk4::Widget,
        @implements gtk4::Buildable, gtk4::Orientable;
}

impl EmulatorWidget for AddressPicker {}

impl AddressPicker {
    async fn set_target_to_pc(self) {
        let emu = self.emu_handle();
        let result = emu.query_registers().await;
        if let Ok(registers) = result {
            self.set_address(registers.read_u16(WordRegister::PC));
        }
    }

    pub fn set_address(&self, value: u16) {
        self.set_property(ADDRESS_PROPERTY, format!("0x{:04X}", value))
    }

    pub fn connect_goto<F>(&self, f: F)
    where
        F: Fn(u16) + 'static,
    {
        self.connect_closure(
            GOTO_ADDRESS_SIGNAL,
            false,
            glib::closure_local!(move |_obj: glib::Object, address: u32| {
                let right_sized = address as u16; // No GValue exists for u16
                f(right_sized);
            }),
        );
    }
}

pub const ADDRESS_PROPERTY: &str = "address";
pub const PC_CLICKED_SIGNAL: &str = "pc-button-clicked";
pub const GOTO_ADDRESS_SIGNAL: &str = "goto-address";

impl ObjectImpl for AddressPickerInternal {
    fn properties() -> &'static [ParamSpec] {
        static PROPERTIES: LazyLock<Vec<ParamSpec>> = LazyLock::new(|| {
            vec![
                ParamSpecString::builder(ADDRESS_PROPERTY)
                    .flags(ParamFlags::READWRITE)
                    .build(),
                emu_param_spec(),
            ]
        });
        PROPERTIES.as_ref()
    }

    fn signals() -> &'static [Signal] {
        static SIGNALS: LazyLock<Vec<Signal>> = LazyLock::new(|| {
            vec![
                Signal::builder(PC_CLICKED_SIGNAL).build(),
                Signal::builder(GOTO_ADDRESS_SIGNAL)
                    .param_types([u32::static_type()])
                    .build(),
            ]
        });
        SIGNALS.as_ref()
    }

    fn constructed(&self) {
        self.parent_constructed();

        self.address_entry.set_text("0x100");
        self.address_selected.store(0x100, Ordering::Relaxed);

        self.address_entry.get().connect_changed(clone!(
            #[to_owned(rename_to = widget)]
            self,
            move |_| {
                let text = widget.address_entry.text();

                if let Ok(addr) = parse_number(&text) {
                    widget.address_selected.store(addr, Ordering::Relaxed);
                    widget.obj().notify(ADDRESS_PROPERTY);
                    widget.go_button.get().set_sensitive(true);
                } else {
                    widget.go_button.get().set_sensitive(false);
                }
            }
        ));

        self.pc_button.connect_clicked(clone!(
            #[to_owned(rename_to = imp)]
            self,
            move |_| {
                glib::MainContext::ref_thread_default()
                    .spawn_local(imp.obj().clone().set_target_to_pc());
            }
        ));
        self.address_entry.connect_activate(clone!(
            #[to_owned(rename_to = imp)]
            self,
            move |_| {
                let address = imp.address_selected.load(Ordering::Relaxed);
                let expanded_address = u32::from(address);
                imp.obj()
                    .emit_by_name::<()>(GOTO_ADDRESS_SIGNAL, &[&expanded_address]);
            }
        ));
        self.go_button.connect_clicked(clone!(
            #[to_owned(rename_to = imp)]
            self,
            move |_| {
                let address = imp.address_selected.load(Ordering::Relaxed);
                let expanded_address = u32::from(address);
                imp.obj()
                    .emit_by_name::<()>(GOTO_ADDRESS_SIGNAL, &[&expanded_address]);
            }
        ));
    }

    fn set_property(&self, _id: usize, value: &glib::Value, pspec: &glib::ParamSpec) {
        match pspec.name() {
            EMU_PROPERTY => {
                self.emu.replace(Some(value.unwrap()));
            }
            ADDRESS_PROPERTY => {
                let address: &str = value.unwrap();
                if let Ok(numeric) = parse_number(address) {
                    self.address_selected.store(numeric, Ordering::Relaxed);
                    self.go_button.get().set_sensitive(true);
                } else {
                    self.go_button.get().set_sensitive(false);
                }
                self.address_entry.set_text(address);
            }
            _ => unimplemented!(),
        }
    }

    // Called whenever a property is retrieved from this instance. The id
    // is the same as the index of the property in the PROPERTIES array.
    fn property(&self, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
        match pspec.name() {
            EMU_PROPERTY => match self.emu.borrow().as_ref() {
                Some(emu) => emu.clone().to_value(),
                None => panic!("No connected emulator"),
            },
            ADDRESS_PROPERTY => u32::from(self.address_selected.load(Ordering::Relaxed)).to_value(),
            _ => unimplemented!(),
        }
    }
}

impl WidgetImpl for AddressPickerInternal {}

impl BoxImpl for AddressPickerInternal {}
