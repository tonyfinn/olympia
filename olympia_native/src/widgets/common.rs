use gtk4::glib::prelude::*;
use gtk4::glib::{self, ParamSpecBuilderExt};

use crate::utils::EmulatorHandle;

pub const EMU_PROPERTY: &str = "emu";

pub trait EmulatorWidget: ObjectExt {
    fn attach_emu(&self, emulator: EmulatorHandle) {
        self.set_property(EMU_PROPERTY, emulator);
    }

    fn emu_handle(&self) -> EmulatorHandle {
        self.property::<EmulatorHandle>(EMU_PROPERTY)
    }
}

pub fn emu_param_spec() -> glib::ParamSpec {
    glib::ParamSpecBoxed::builder::<EmulatorHandle>(EMU_PROPERTY)
        .flags(glib::ParamFlags::READWRITE)
        .build()
}
