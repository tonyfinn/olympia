use crate::builder_struct;
use crate::utils;

use derive_more::{Display, Error, From, Into};
use gtk4::gio;
use gtk4::glib;
use gtk4::glib::clone;
use gtk4::glib::prelude::*;
use gtk4::glib::Properties;
use gtk4::prelude::*;
use gtk4::StringObject;
use olympia_engine::monitor::BreakpointCondition;
use olympia_engine::monitor::BreakpointIdentifier;
use olympia_engine::monitor::Comparison;
use olympia_engine::{
    monitor::{Breakpoint, RWTarget},
    remote::RemoteEmulator,
};
use std::cell::RefCell;
use std::rc::Rc;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::AtomicU32;

builder_struct!(
    pub(crate) struct BreakpointViewerWidget {
        #[ogtk(id = "DebuggerBreakpointAdd")]
        add_button: gtk4::Button,
        #[ogtk(id = "DebuggerBreakpointMonitorEntry")]
        monitor_input: gtk4::Entry,
        #[ogtk(id = "BreakpointColumnView")]
        breakpoint_list: gtk4::ColumnView,
        #[ogtk(id = "DebuggerConditionPicker")]
        condition_picker: gtk4::DropDown,
        #[ogtk(id = "DebuggerExpectedValueEntry")]
        value_input: gtk4::Entry,
    }
);

#[derive(PartialEq, Eq, Clone, Debug, Display, Error)]
pub enum BreakpointParseError {
    #[display("Invalid Target {0:?}", _0)]
    Target(#[error(not(source))] String),
    #[display("Invalid value {0:?}", _0)]
    Value(#[error(not(source))] String),
    #[display("Invalid target {0:?} and invalid value {1:?}", _0, _1)]
    TargetAndValue(String, String),
}

#[derive(Clone, Debug, PartialEq, Eq, glib::Boxed, From, Into)]
#[boxed_type(name = "boxed_identifier")]
struct BoxedIdentifier(BreakpointIdentifier);

pub mod imp {
    use super::*;
    use glib::subclass::prelude::*;

    #[derive(Properties, Default)]
    #[properties(wrapper_type = super::ListBreakpoint)]
    pub struct ListBreakpoint {
        #[property(get, set)]
        id: AtomicU32,
        #[property(get, set)]
        condition: RefCell<String>,
        #[property(get, set)]
        monitor: RefCell<String>,
        #[property(get, set)]
        active: AtomicBool,
    }

    #[glib::derived_properties]
    impl ObjectImpl for ListBreakpoint {}

    #[glib::object_subclass]
    impl ObjectSubclass for ListBreakpoint {
        const NAME: &'static str = "ListBreakpoint";
        type Type = super::ListBreakpoint;
    }
}

glib::wrapper! {
    pub struct ListBreakpoint(ObjectSubclass<imp::ListBreakpoint>);
}

impl ListBreakpoint {
    fn new(identifier: BreakpointIdentifier, bp: &Breakpoint) -> ListBreakpoint {
        let list_bp: ListBreakpoint = glib::object::Object::new();
        list_bp.set_id(u32::from(identifier));
        list_bp.set_active(bp.active);
        list_bp.set_condition(format!("{}", bp.condition));
        list_bp.set_monitor(format!("{}", bp.monitor));
        list_bp
    }

    fn identifier(&self) -> BreakpointIdentifier {
        BreakpointIdentifier::from(self.id())
    }
}

impl Default for BoxedIdentifier {
    fn default() -> Self {
        BoxedIdentifier(BreakpointIdentifier::new(0))
    }
}

pub(crate) struct BreakpointViewer {
    context: glib::MainContext,
    emu: Rc<RemoteEmulator>,
    widget: BreakpointViewerWidget,
    store: gio::ListStore,
}

impl BreakpointViewer {
    pub(crate) fn from_widget(
        context: glib::MainContext,
        emu: Rc<RemoteEmulator>,
        widget: BreakpointViewerWidget,
    ) -> Rc<BreakpointViewer> {
        let store = gio::ListStore::with_type(ListBreakpoint::static_type());
        let selection_model = gtk4::SingleSelection::new(Some(store.clone()));

        let bpv = Rc::new(BreakpointViewer {
            context,
            emu,
            widget,
            store: store.clone(),
        });

        bpv.widget.breakpoint_list.set_model(Some(&selection_model));
        bpv.create_text_column(
            "Target",
            Box::new(|bp: &ListBreakpoint| bp.monitor().clone()),
        );
        bpv.create_text_column(
            "Condition",
            Box::new(|bp: &ListBreakpoint| bp.condition().clone()),
        );
        bpv.create_active_column("Active");

        bpv.connect_ui_events();
        bpv
    }

    pub(crate) fn from_builder(
        builder: &gtk4::Builder,
        context: glib::MainContext,
        emu: Rc<RemoteEmulator>,
    ) -> Rc<BreakpointViewer> {
        let widget = BreakpointViewerWidget::from_builder(builder).unwrap();
        BreakpointViewer::from_widget(context, emu, widget)
    }

    fn add_clicked(self: Rc<Self>) {
        self.context.spawn_local(self.clone().add_breakpoint());
    }

    fn create_active_column(self: &Rc<Self>, title: &str) {
        let signal_item_factory = gtk4::SignalListItemFactory::new();
        signal_item_factory.connect_setup(move |_factory, obj| {
            let list_item = obj.downcast_ref::<gtk4::ListItem>().unwrap().clone();
            list_item.set_child(Some(&gtk4::CheckButton::new()));
        });
        signal_item_factory.connect_bind(clone!(
            #[strong(rename_to = bpv)]
            self,
            move |_factory, obj| {
                let list_item = obj.downcast_ref::<gtk4::ListItem>().unwrap().clone();
                let child: gtk4::CheckButton = list_item.child().and_downcast().unwrap();
                let bp: ListBreakpoint = list_item.item().unwrap().downcast().unwrap();
                child.set_active(bp.active());
                child.connect_toggled(clone!(
                    #[strong]
                    bpv,
                    #[strong]
                    bp,
                    move |cb| {
                        tracing::debug!(?bp, "Breakpoint checkbox toggled");
                        bpv.context.spawn_local(
                            bpv.clone()
                                .toggle_breakpoint(bp.identifier(), cb.is_active()),
                        );
                    }
                ));
            }
        ));
        let column = gtk4::ColumnViewColumn::builder()
            .factory(&signal_item_factory)
            .title(title)
            .expand(true)
            .build();
        self.widget.breakpoint_list.append_column(&column);
    }

    fn create_text_column(self: &Rc<Self>, title: &str, f: Box<dyn Fn(&ListBreakpoint) -> String>) {
        let signal_item_factory = gtk4::SignalListItemFactory::new();
        signal_item_factory.connect_setup(|_factory, obj| {
            let list_item = obj.downcast_ref::<gtk4::ListItem>().unwrap().clone();
            list_item.set_child(Some(&gtk4::Label::new(None)));
        });
        signal_item_factory.connect_bind(move |_factory, obj| {
            let list_item = obj.downcast_ref::<gtk4::ListItem>().unwrap().clone();
            let child: gtk4::Label = list_item.child().and_downcast().unwrap();
            let bp: ListBreakpoint = list_item.item().unwrap().downcast().unwrap();
            child.set_label(&f(&bp));
        });
        let column = gtk4::ColumnViewColumn::builder()
            .factory(&signal_item_factory)
            .title(title)
            .build();
        self.widget.breakpoint_list.append_column(&column);
    }

    fn find_list_bp(&self, id: BreakpointIdentifier) -> Option<ListBreakpoint> {
        self.store.iter::<ListBreakpoint>().find_map(|item| {
            if let Ok(candidate) = item {
                if candidate.identifier() == id {
                    Some(candidate)
                } else {
                    None
                }
            } else {
                None
            }
        })
    }

    async fn toggle_breakpoint(self: Rc<Self>, id: BreakpointIdentifier, new_state: bool) {
        if let Some(found_bp) = self.find_list_bp(id) {
            let previous_state = found_bp.active();
            let result = self.emu.set_breakpoint_state(id, new_state).await;
            if let Ok(resp) = result {
                tracing::debug!(previous_state, new_state, "Toggled breakpoint");
                found_bp.set_active(resp.new_state);
            }
        }
    }

    fn condition_changed(self: &Rc<Self>) {
        let selection = self.widget.condition_picker.selected_item();

        if let Some(selection) = selection {
            if let Some(choice) = selection.downcast_ref::<gtk4::StringObject>() {
                let choice_str = choice.string();
                tracing::debug!(%choice_str, "Condition changed");
                let has_value = !matches!(choice_str.as_str(), "Read" | "Write");
                self.widget.value_input.set_visible(has_value);
            }
        }
    }

    pub fn connect_ui_events(self: &Rc<Self>) {
        self.widget.add_button.connect_clicked(clone!(
            #[weak(rename_to = bpv)]
            self,
            move |_| {
                bpv.add_clicked();
            }
        ));

        self.widget.condition_picker.connect_selected_notify(clone!(
            #[weak(rename_to = bpv)]
            self,
            move |_| {
                bpv.condition_changed();
            }
        ));
    }

    fn parse_breakpoint(&self) -> Result<Breakpoint, BreakpointParseError> {
        let target_text: String = self.widget.monitor_input.text().into();
        let target: Option<RWTarget> = target_text.parse().ok();
        let value_text: String = self.widget.value_input.text().into();
        let value = if let Some(RWTarget::Cycles) = target {
            let (num, multiplier) = if value_text.ends_with('s') {
                let s = value_text.replace("s", "");
                (String::from(s.as_str()), 1024 * 1024)
            } else {
                (value_text.clone(), 1)
            };
            u64::from_str_radix(&num, 16).ok().map(|x| x * multiplier)
        } else {
            u64::from_str_radix(&value_text, 16).ok()
        };
        let picker = &self.widget.condition_picker;
        let condition = picker.selected_item().and_then(|s| {
            let gstring = s
                .downcast::<StringObject>()
                .expect("Wrong type from picker")
                .string();
            let comparison: Result<Comparison, _> = gstring.parse();
            if let Ok(comp) = comparison {
                let expected_value = value?;
                Some(BreakpointCondition::Test(comp, expected_value))
            } else {
                match gstring.as_str() {
                    "Read" => Some(BreakpointCondition::Read),
                    "Write" => Some(BreakpointCondition::Write),
                    _ => None,
                }
            }
        });
        match (target, condition) {
            (Some(t), Some(c)) => Ok(Breakpoint::new(t, c)),
            (None, Some(_cond)) => Err(BreakpointParseError::Target(target_text)),
            (Some(_target), None) => Err(BreakpointParseError::Value(value_text)),
            (None, None) => Err(BreakpointParseError::TargetAndValue(
                target_text,
                value_text,
            )),
        }
    }

    async fn add_parsed_breakpoint(&self, breakpoint: &Breakpoint) {
        let resp = utils::run_infallible(self.emu.add_breakpoint(breakpoint.clone())).await;
        let list_bp = ListBreakpoint::new(resp.id, breakpoint);
        self.store.append(&list_bp);
    }

    async fn add_breakpoint(self: Rc<Self>) {
        match self.parse_breakpoint() {
            Ok(bp) => self.add_parsed_breakpoint(&bp).await,
            Err(err) => {
                utils::show_error_dialog(err, None).await;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use glib::Object;

    use super::*;
    use crate::utils::test_utils;

    fn add_breakpoint(
        component: Rc<BreakpointViewer>,
        context: &glib::MainContext,
        emu: Rc<RemoteEmulator>,
        monitor: &str,
        condition: &str,
        value: Option<&str>,
    ) {
        component.widget.monitor_input.set_text(monitor);
        if let Some(v) = value {
            component.widget.value_input.set_text(v);
        }
        let condition_picker = &component.widget.condition_picker;
        let store = condition_picker.model().unwrap();
        let position = store
            .iter::<Object>()
            .position(|sobj| match sobj.ok().and_downcast::<StringObject>() {
                Some(sobj) => sobj.string().as_str() == condition,
                _ => false,
            })
            .unwrap_or_else(|| panic!("Condition {} not found", condition));
        component
            .widget
            .condition_picker
            .set_selected(position as u32);
        component.widget.add_button.emit_clicked();
        test_utils::next_tick(context, &emu)
    }

    #[test]
    fn test_add_breakpoint() {
        test_utils::with_loaded_emu(|context, emu| {
            let builder = gtk4::Builder::from_string(include_str!("../../res/breakpoints.ui"));
            let component = BreakpointViewer::from_builder(&builder, context.clone(), emu.clone());
            add_breakpoint(
                component.clone(),
                &context,
                emu.clone(),
                "PC",
                "==",
                Some("20"),
            );

            add_breakpoint(component.clone(), &context, emu, "AF", "!=", Some("40"));

            let count = component.store.n_items();
            assert_eq!(count, 2);
            let first = component
                .store
                .item(0)
                .unwrap()
                .downcast::<ListBreakpoint>()
                .unwrap();
            assert!(first.active());
            assert_eq!(&first.monitor(), "register PC");
            assert_eq!(&first.condition(), "== 20");
        });
    }

    /*
    #[test]
    fn test_toggle_breakpoint() {
        test_utils::with_loaded_emu(|context, emu| {
            let builder = gtk4::Builder::from_string(include_str!("../../res/breakpoints.ui"));
            let component = BreakpointViewer::from_builder(&builder, context.clone(), emu.clone());
            add_breakpoint(
                component.clone(),
                &context,
                emu.clone(),
                "PC",
                "==",
                Some("20"),
            );
            add_breakpoint(
                component.clone(),
                &context,
                emu.clone(),
                "AF",
                "!=",
                Some("40"),
            );
            component
                .widget
                .breakpoint_list
                .emit_by_name::<()>("toggled", &[&tree_path.to_str()]);
            test_utils::next_tick(&context, &emu);

            let count = component.store.n_items();
            assert_eq!(count, 2, "wrong number of items in tree");
            let iter = store.iter_first().unwrap();
            store.iter_next(&iter);
            let active: bool = store.value(&iter, ACTIVE_COLUMN_INDEX).get().unwrap();
            assert!(!active);
        });
    } */
}
