use gtk4::glib;
use gtk4::glib::clone;

use olympia_engine::{
    events::{propagate_events, EventEmitter, ModeChangeEvent},
    gameboy::{GameBoy, StepError, CYCLE_FREQ},
    monitor::{BreakpointState, DebugMonitor},
    remote::{
        self, CommandId, EmulatorCommand, EmulatorResponse, EmulatorState, ExecMode, LoadRomError, RemoteEmulatorError, RemoteEmulatorOutput, ToggleBreakpointResponse
    },
};
use tokio::sync::mpsc::{unbounded_channel, UnboundedReceiver, UnboundedSender};

use std::sync::mpsc;
use std::thread;
use std::time::{Duration, Instant};
use std::{cell::RefCell, rc::Rc};

struct SenderClosed {}

pub(super) struct EmulatorThread {
    state: EmulatorState,
    rx: mpsc::Receiver<(CommandId, EmulatorCommand)>,
    tx: Rc<UnboundedSender<RemoteEmulatorOutput>>,
    events: Rc<EventEmitter<remote::Event>>,
    exec_mode: ExecMode,
}

impl EmulatorThread {
    fn new(
        command_rx: mpsc::Receiver<(CommandId, EmulatorCommand)>,
        event_tx: UnboundedSender<RemoteEmulatorOutput>,
    ) -> EmulatorThread {
        let state = EmulatorState::new();
        EmulatorThread {
            state,
            rx: command_rx,
            tx: Rc::new(event_tx),
            events: Rc::new(EventEmitter::new()),
            exec_mode: ExecMode::Unloaded,
        }
    }

    pub fn start() -> (
        thread::JoinHandle<()>,
        mpsc::Sender<(CommandId, EmulatorCommand)>,
        UnboundedReceiver<RemoteEmulatorOutput>,
    ) {
        let (command_tx, command_rx) = mpsc::channel();
        let (event_tx, event_rx) = unbounded_channel();

        let thread = thread::spawn(move || {
            let emu_thread = EmulatorThread::new(command_rx, event_tx);
            emu_thread
                .events
                .on(Box::new(clone!(
                    #[weak(rename_to = tx)]
                    emu_thread.tx, 
                        move |evt| {
                        if let Err(err) = tx.send(RemoteEmulatorOutput::Event(evt.clone())) {
                            tracing::error!(target: "emu_thread", ?err, ?evt, "Cannot report emulator output event");
                        }
                    })
                ));
            emu_thread.run();
        });

        (thread, command_tx, event_rx)
    }

    fn load_rom(
        state: &mut EmulatorState,
        events: Rc<EventEmitter<remote::Event>>,
        data: Vec<u8>,
    ) -> Result<(), LoadRomError> {
        state.load_rom(data)?;
        if let Some(ref gb) = state.gameboy {
            propagate_events(&gb.events, events);
        } else {
            panic!("Gameboy not present after ROM loaded");
        }
        Ok(())
    }

    fn handle_commands(&mut self) -> Result<(), SenderClosed> {
        for (id, cmd) in self.rx.try_iter() {
            let resp: EmulatorResponse = match cmd {
                EmulatorCommand::LoadRom(data) => {
                    let resp = EmulatorResponse::LoadRom(EmulatorThread::load_rom(
                        &mut self.state,
                        self.events.clone(),
                        data,
                    ));
                    self.exec_mode = ExecMode::Paused;
                    self.tx
                        .send(RemoteEmulatorOutput::Event(
                            ModeChangeEvent::new(ExecMode::Unloaded, ExecMode::Paused).into(),
                        ))
                        .map_err(|_| SenderClosed {})?;
                    resp
                }
                EmulatorCommand::QueryMemory(start_index, end_index) => {
                    EmulatorResponse::QueryMemory(self.state.query_memory(start_index, end_index))
                }
                EmulatorCommand::QueryRegisters => {
                    EmulatorResponse::QueryRegisters(self.state.query_registers())
                }
                EmulatorCommand::Step => EmulatorResponse::Step(self.state.step()),
                EmulatorCommand::QueryBankConfig => EmulatorResponse::QueryBankConfig(self.state.query_bank_config()),
                EmulatorCommand::QueryExecTime => {
                    EmulatorResponse::QueryExecTime(self.state.exec_time())
                }
                EmulatorCommand::SetMode(mode) => {
                    if mode == ExecMode::Standard || mode == ExecMode::Uncapped {
                        self.state.monitor.borrow_mut().resume();
                    }
                    let old_mode = self.exec_mode.clone();
                    self.exec_mode = mode;
                    self.tx
                        .send(RemoteEmulatorOutput::Event(
                            ModeChangeEvent::new(old_mode, self.exec_mode.clone()).into(),
                        ))
                        .map_err(|_| SenderClosed {})?;
                    EmulatorResponse::SetMode(Ok(self.exec_mode.clone()))
                }
                EmulatorCommand::AddBreakpoint(bp) => {
                    let resp = self.state.monitor.borrow_mut().add_breakpoint(bp);
                    EmulatorResponse::AddBreakpoint(Ok(resp.into()))
                }
                EmulatorCommand::RemoveBreakpoint(id) => {
                    let resp = self.state.monitor.borrow_mut().remove_breakpoint(id);
                    if resp.is_none() {
                        tracing::info!(
                            breakpoint_id = ?id,
                            "Tried to remove invalid breakpoint"
                        );
                    }
                    EmulatorResponse::RemoveBreakpoint(Ok(id.into()))
                }
                EmulatorCommand::SetBreakpointActive(id, state) => {
                    let resp = self
                        .state
                        .monitor
                        .borrow_mut()
                        .set_breakpoint_state(id, state);
                    if let Some(state) = resp {
                        EmulatorResponse::ToggleBreakpoint(Ok(ToggleBreakpointResponse::new(
                            id, state,
                        )))
                    } else {
                        EmulatorResponse::ToggleBreakpoint(Err(()))
                    }
                }
            };
            self.tx
                .send(RemoteEmulatorOutput::Response(id, resp))
                .map_err(|_| SenderClosed {})?;
        }
        Ok(())
    }

    fn step(
        gb: &mut GameBoy,
        monitor: &RefCell<DebugMonitor>,
        inital_mode: ExecMode,
    ) -> Result<ExecMode, StepError> {
        gb.step()?;
        if let BreakpointState::HitBreakpoint(bp) = monitor.borrow().state() {
            tracing::info!(target: "emu_thread", breakpoint = ?bp, "Hit breakpoint");
            return Ok(ExecMode::HitBreakpoint(bp));
        }
        Ok(inital_mode)
    }

    fn run(mut self) {
        loop {
            if self.handle_commands().is_err() {
                break;
            }
            if let Some(gb) = self.state.gameboy.as_mut() {
                let start_time = Instant::now();
                let result = match &self.exec_mode {
                    ExecMode::Paused | ExecMode::Unloaded | ExecMode::HitBreakpoint(_) => {
                        thread::sleep(Duration::from_micros(10000));
                        Ok(self.exec_mode.clone())
                    }
                    ExecMode::Standard => {
                        thread::sleep(Duration::from_secs_f64(1.0 / (f64::from(CYCLE_FREQ))));
                        let step_result =
                            EmulatorThread::step(gb, &self.state.monitor, self.exec_mode.clone());
                        gb.add_exec_time(start_time.elapsed().as_secs_f64());
                        step_result
                    }
                    ExecMode::Uncapped => {
                        let step_result =
                            EmulatorThread::step(gb, &self.state.monitor, self.exec_mode.clone());
                        gb.add_exec_time(start_time.elapsed().as_secs_f64());
                        step_result
                    }
                };
                match result {
                    Err(e) => {
                        self.tx
                            .send(RemoteEmulatorError::Exec(e).into())
                            .expect("Emulator thread response channel closed");
                    }
                    Ok(mode) => {
                        if mode != self.exec_mode {
                            let change_event =
                                ModeChangeEvent::new(self.exec_mode.clone(), mode.clone());
                            self.exec_mode = mode;
                            self.tx
                                .send(remote::RemoteEmulatorOutput::Event(change_event.into()))
                                .expect("Emulator thread response channel closed");
                        }
                    }
                }
            } else {
                thread::sleep(Duration::from_micros(10000))
            }
        }
    }
}
