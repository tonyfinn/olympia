use gtk4::{
    gdk, gio,
    glib::{self, clone},
    prelude::*,
    Application, ApplicationWindow, CssProvider, STYLE_PROVIDER_PRIORITY_APPLICATION,
};
use std::path::PathBuf;
use std::rc::Rc;

use crate::emulator::glib::glib_remote_emulator;
use crate::utils;
use crate::widgets::{
    common::EmulatorWidget, BreakpointViewer, Disassembler, EmulatorDisplay, MemoryViewer,
    PlaybackControls, RegisterLabels, TilesetViewer,
};

use olympia_engine::remote::{LoadRomError, RemoteEmulator};

#[allow(dead_code)]
pub(crate) struct Debugger {
    emu: Rc<RemoteEmulator>,
    breakpoint_viewer: Rc<BreakpointViewer>,
    disassembler: Disassembler,
    emulator_display: Rc<EmulatorDisplay>,
    memory_viewer: Rc<MemoryViewer>,
    register_labels: Rc<RegisterLabels>,
    playback_controls: Rc<PlaybackControls>,
    window: ApplicationWindow,
}

fn create_child<C: IsA<gtk4::Widget> + IsA<glib::Object>>(
    parent_builder: &gtk4::Builder,
    builder_xml: &str,
    container_id: &str,
    content_id: &str,
) -> gtk4::Builder {
    let child_builder = gtk4::Builder::from_string(builder_xml);
    let container: gtk4::Box = parent_builder.object(container_id).unwrap();
    let content: C = child_builder.object(content_id).unwrap();
    content.set_halign(gtk4::Align::Fill);
    content.set_valign(gtk4::Align::Fill);
    container.append(&content);
    child_builder
}

impl Debugger {
    pub(crate) fn new(app: &Application) -> Rc<Debugger> {
        let ctx = glib::MainContext::ref_thread_default();
        let emu = glib_remote_emulator(ctx.clone());

        let root_builder = gtk4::Builder::from_string(include_str!("../../res/debugger.ui"));

        let playback_controls =
            PlaybackControls::from_builder(&root_builder, ctx.clone(), emu.clone());
        let window: ApplicationWindow = root_builder.object("MainWindow").unwrap();
        let css_provider = CssProvider::new();
        css_provider.load_from_string(include_str!("../../res/style.css"));
        gtk4::style_context_add_provider_for_display(
            &gdk::Display::default().expect("Could not get root display"),
            &css_provider,
            STYLE_PROVIDER_PRIORITY_APPLICATION,
        );
        let open_action = gio::SimpleAction::new("open", None);
        let emulator_display =
            EmulatorDisplay::from_builder(&root_builder, ctx.clone(), emu.clone());

        let register_builder = create_child::<gtk4::Box>(
            &root_builder,
            include_str!("../../res/registers.ui"),
            "RegistersContainer",
            "Registers",
        );
        let register_labels =
            RegisterLabels::from_builder(&register_builder, ctx.clone(), emu.clone());

        let memv_builder = create_child::<gtk4::Box>(
            &root_builder,
            include_str!("../../res/memory.ui"),
            "MemoryContainer",
            "Memory",
        );
        let memory_viewer = MemoryViewer::from_builder(&memv_builder, ctx.clone(), emu.clone(), 17);

        let disassembler: Disassembler = root_builder.object("Disassembler").unwrap();
        disassembler.attach_emu(emu.clone().into());

        let tileset_viewer: TilesetViewer = root_builder.object("TilesetViewer").unwrap();
        tileset_viewer.attach_emu(emu.clone().into());

        let bpv_builder = create_child::<gtk4::Box>(
            &root_builder,
            include_str!("../../res/breakpoints.ui"),
            "BreakpointsContainer",
            "Breakpoints",
        );
        let breakpoint_viewer =
            BreakpointViewer::from_builder(&bpv_builder, ctx.clone(), emu.clone());

        window.set_application(Some(app));
        window.add_action(&open_action);
        window.set_show_menubar(true);

        let debugger = Rc::new(Debugger {
            emu,
            breakpoint_viewer,
            disassembler,
            emulator_display,
            memory_viewer,
            playback_controls,
            register_labels,
            window: window.clone(),
        });

        open_action.connect_activate(clone!(
            #[strong]
            debugger,
            #[strong]
            window,
            #[strong]
            ctx,
            move |_, _| {
                gtk4::FileDialog::builder()
                    .title("Load ROM")
                    .accept_label("Open")
                    .build()
                    .open(
                        Some(&window),
                        gio::Cancellable::NONE,
                        clone!(
                            #[strong]
                            debugger,
                            #[strong]
                            ctx,
                            move |file| {
                                if let Some(path) = file.map(|f| f.path()).ok().flatten() {
                                    ctx.spawn_local(debugger.clone().load_rom(path));
                                }
                            }
                        ),
                    );
            }
        ));

        debugger
    }

    async fn load_rom_fs(&self, path: PathBuf) -> Result<(), LoadRomError> {
        let data = std::fs::read(path).map_err(|err| LoadRomError::Io(format!("{}", err)))?;
        self.emu.load_rom(data).await
    }

    async fn load_rom(self: Rc<Self>, path: PathBuf) {
        let res = utils::run_fallible(self.load_rom_fs(path), Some(&self.window)).await;
        if let Err(err) = res {
            tracing::error!(?err, "Failed to load rom");
        }
    }

    pub(crate) fn show_all(&self) {
        self.window.set_visible(true);
    }
}
