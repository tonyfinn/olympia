use std::{fmt::Display, str::FromStr};

use derive_more::derive::{Display, Error};

#[derive(Clone, Debug, Display, Error)]
pub(crate) enum ExprParseError {
    #[display("Could not parse the literal value: {:?}", _0)]
    ParseError(syn::parse::Error),
    #[display("Expression not of correct type: {:?}", _0)]
    WrongExprType(#[error(not(source))] syn::Expr),
}

pub(crate) fn parse_int_expr<T>(expr: &syn::Expr) -> Result<T, ExprParseError>
where
    T: FromStr,
    T::Err: Display,
{
    match expr {
        syn::Expr::Lit(lit_expr) => {
            if let syn::Lit::Int(val) = &lit_expr.lit {
                val.base10_parse().map_err(ExprParseError::ParseError)
            } else {
                Err(ExprParseError::WrongExprType(expr.clone()))
            }
        }
        syn::Expr::Group(group_expr) => parse_int_expr(&group_expr.expr),
        _ => Err(ExprParseError::WrongExprType(expr.clone())),
    }
}

pub(crate) fn parse_str_expr(expr: &syn::Expr) -> Result<String, ExprParseError> {
    match expr {
        syn::Expr::Lit(lit_expr) => {
            if let syn::Lit::Str(val) = &lit_expr.lit {
                Ok(val.value())
            } else {
                Err(ExprParseError::WrongExprType(expr.clone()))
            }
        }
        syn::Expr::Group(group_expr) => parse_str_expr(&group_expr.expr),
        _ => Err(ExprParseError::WrongExprType(expr.clone())),
    }
}
